﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Newtonsoft.Json.Linq;
using pj = org.pjsip.pjsua2;

using hesong.plum.client;
using hesong.plum.client.wpf.Common;
using hesong.plum.client.wpf.MyClass;
using hesong.plum.client.wpf.Tools;
using System.Media;
using hesong.plum.client.wpf.Utils;
using System.IO;
using System.Windows.Markup;

namespace hesong.plum.client.wpf
{
    /// <summary>
    /// ToolBar.xaml 的交互逻辑
    /// </summary>
    public partial class ToolBar : Window
    {
        SolidColorBrush colorImgEnable = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF40B0FF"));//登录成功后矢量图的颜色
        SolidColorBrush colorImgDisable = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFB0BBC1"));//登录前的控件颜色

        SolidColorBrush colorFontEnable = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF6E91A5"));//登录成功后字体颜色
        SolidColorBrush colorFontDisable = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFB0BBC1"));//控件停用时的颜色

        SolidColorBrush colorEnter = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF3E79D3"));//鼠标进入相关控件后的颜色

        private object lockFix = new object();//固定工具条的锁
        System.Timers.Timer t = new System.Timers.Timer();//通话计时定时器
        /// <summary>
        /// 振铃时状态栏颜色变换定时器
        /// </summary>
        System.Timers.Timer tRing = new System.Timers.Timer();
        DateTime statusBeinTime = DateTime.Now;//状态开始时，用于状态计时


        System.Timers.Timer tTooltip = new System.Timers.Timer();
        DateTime toolTipDisplayTime = DateTime.Now;//提示框显示时间
        public ToolBar()
        {
            InitializeComponent();
            _instance = this;
            Sip.Utils.Startup();
            setPosition();
            setTimer();
            setLogoutStyle();
            setInfoBar(InfoBarStyle.Default, false);
            setInfoBar(InfoBarStyle.Default, true, "尚未登录");
            fixToolbar();
            setButtonImage();
            showDialog();
            TmpCloseCallList();

            #region 此部分为测试代码

        
            //setLoginSuccessStyle();
            //refreshStatus(AgentState.Distributed);
            // setInfoBarBottomMsg("15915915195");
            //AcdInfo acd = new AcdInfo();
            //acd.Id = System.Guid.NewGuid().ToString();
            //acd.TargetLineTelnum = "15915915195";
            //holdListAdd(acd);
            //controlButtonHoldList(true);
            //controlLogoutButton(true);
            //new Thread(() =>
            //{

              
            //    //while (true)
            //    //{
            //    //    Thread.Sleep(1000);
            //    //    var toolbar = ToolBar.instance;
            //    //    toolbar.Dispatcher.Invoke(new Action(() =>
            //    //    {
            //    //        toolbar.setToolTip("连接座席分机失败：呼叫失败");
            //    //    }));
            //    //    Thread.Sleep(15000);
            //    //    toolbar.Dispatcher.Invoke(new Action(() =>
            //    //    {
            //    //        toolbar.setToolTip("连接座席分机失败：");
            //    //    }));
            //    //    Thread.Sleep(10000);
            //    //    toolbar.Dispatcher.Invoke(new Action(() =>
            //    //    {
            //    //        toolbar.setToolTip("连接座席分机失败：连接座席分机失败");
            //    //    }));
            //    //    Thread.Sleep(3000);
            //    //    toolbar.Dispatcher.Invoke(new Action(() =>
            //    //    {
            //    //        toolbar.setToolTip("连接座席分机失败：呼叫败");
            //    //    }));
            //    //    Thread.Sleep(7000);
            //    //    toolbar.Dispatcher.Invoke(new Action(() =>
            //    //    {
            //    //        toolbar.setToolTip("连接座席分机");
            //    //    }));
            //    //}

            //}).Start();



            #endregion

        }
        /// <summary>
        /// 临时隐藏呼叫窗口中的座席和技能列表
        /// </summary>
        private void TmpCloseCallList()
        {

            //popup_call.Height = 100;
            //Grid_call.RowDefinitions[1].Height = new GridLength(0);
            //Grid_call.RowDefinitions[2].Height = new GridLength(0);
            //Grid_call.RowDefinitions[3].Height = new GridLength(0);
        }
        private void setButtonImage()
        {

            button_queue_monitor.SetImage(Properties.Resources.queue_enable, Properties.Resources.queue_disable);
            button_agent_monitor.SetImage(Properties.Resources.monitor_enable, Properties.Resources.monitor_disable);
            button_config.SetImage(Properties.Resources.settings_enable, Properties.Resources.settings_disable);
            button_logout.SetImage(Properties.Resources.logout_enable, Properties.Resources.logout_disable);
            button_exit.SetImage(Properties.Resources.exit_enable, Properties.Resources.exit_disable);

        }

        internal void showDialog()
        {
            if (!VersionChecking.Start())
            {
                if (!ToolBarConfig.SipIsConfigurated())
                {
                    showToolBarConfig(0);
                }
                else
                {

                    if (agent == null)
                    {
                        if (Properties.Settings.Default.displayLoginBox)
                        {
                            showLoginBox();
                        }
                    }
                }

            }
        }

        static ToolBar _instance = null;
        internal static ToolBar instance
        {
            get
            {
                return _instance;
            }
        }
        private void setTimer()
        {
            t.Enabled = false;
            t.Interval = 1000;
            t.Elapsed += T_Elapsed;
            tRing.Enabled = false;
            tRing.Interval = 500;
            tRing.Elapsed += T_Elapsed1;
            tTooltip.Enabled = false;
            tTooltip.Interval = 1000;
            tTooltip.Elapsed += TTooltip_Elapsed;
        }



        private void startCallTime()
        {
            if (ringTime)
            {
                return;
            }
            tb_timelen.Text = "00:00";
            statusBeinTime = DateTime.Now;
            t.Enabled = true;
            tb_timelen.Visibility = Visibility.Visible;
        }
        private void stopCallTime()
        {
            t.Enabled = false;
            // tb_timelen.Text = "00:00";
            tb_timelen.Visibility = Visibility.Hidden;
        }

        private void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            TimeSpan ts = DateTime.Now.Subtract(statusBeinTime);
            string tl = ts.ToString(@"mm\:ss");
            tb_timelen.Dispatcher.BeginInvoke(new Action(() => { tb_timelen.Text = tl; }));

        }
        bool ringColor = false;
        /// <summary>
        /// 振铃状态计时器辅助开关标志,true-继续计时，false-重新计时
        /// </summary>
        bool ringTime = false;
        private void T_Elapsed1(object sender, ElapsedEventArgs e)
        {
            try
            {
                string statusStr = "话机振铃";
                var style = ringColor ? InfoBarStyle.Primary : InfoBarStyle.Warning;
                ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                {
                    ToolBar.instance.setInfoBar(style, true, statusStr);
                }));
                ringColor = !ringColor;
                ringTime = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

        }
        private void controlRingTimer(bool isEnable)
        {
            ringTime = false;
            if (isEnable)
            {
                ringColor = true;
            }
            if (tRing.Enabled)
            {
                tRing.Enabled = isEnable;
                Thread.Sleep(100);//防止振铃结束时定时器还没结束，状态栏仍然被定时器设置为振铃
            }
            else
            {
                tRing.Enabled = isEnable;
            }

        }


        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // 获取鼠标相对标题栏位置  
            Point position = e.GetPosition(border_position);


            // 如果鼠标位置在标题栏内，允许拖动  
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (position.X >= 0 && position.X < border_position.ActualWidth && position.Y >= 0 && position.Y < border_position.ActualHeight)
                {
                    this.DragMove();
                }
            }
        }






        private Login loginBox = null;//登录窗体对象
        private void Border_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ToolBar.userOperatorLog("弹出登录框");
            //if (agent != null && !agent.Connected)
            //{
            //    if (loginBox != null)
            //    {
            //        loginBox.PerformLogin();
            //    }
            //    return;
            //}
            //agent = null;
            showLoginBox();
        }

        internal void showLoginBox()
        {
            if (agent != null && agent.Connected)
                return;
            if (loginBox == null)
            {
                loginBox = new Login();
            }
            loginBox.bt_login.IsEnabled = true;
            loginBox.show();
        }

        private void Border_MouseEnter(object sender, MouseEventArgs e)
        {

            (sender as Border).Background = colorEnter;
        }

        private void Border_MouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Border).Background = colorImgEnable;

        }
        private void border_check_in_out_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Border).Background = colorEnter;

        }

        private void border_check_in_out_MouseLeave(object sender, MouseEventArgs e)
        {

            (sender as Border).Background = colorImgEnable;
        }
        bool isCheckIn = false;
        private void border_check_in_out_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (agent != null)
            {
                isCheckIn = !isCheckIn;
                agent.CheckAllSkills(isCheckIn);
                refreshCheckInOut();
            }
        }

        private void refreshCheckInOut()
        {

            if (isCheckIn)
            {
                tb_check_in_out.Text = "已签入";
            }
            else
            {
                tb_check_in_out.Text = "未签入";
            }
        }
        /// <summary>
        /// 隐藏toolbar控件右侧的下拉箭头
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolBar_Loaded_1(object sender, RoutedEventArgs e)
        {
            // ToolBar toolBar = sender as ToolBar;

            var overflowGrid = myToolbar.Template.FindName("OverflowGrid", myToolbar) as FrameworkElement;

            if (overflowGrid != null)

            {

                overflowGrid.Visibility = Visibility.Collapsed;

            }
            var mainPanelBorder = myToolbar.Template.FindName("MainPanelBorder", myToolbar) as FrameworkElement;

            if (mainPanelBorder != null)
            {
                mainPanelBorder.Margin = new Thickness(0);
            }
        }

        /// <summary>
        /// 登录成功后设置相关控件的样式
        /// </summary>
        public void LoginSuccess()
        {

            getAgentInfo();
            RefreshSkillList();
            if (agent.Skills.Any(m => m.Belonged == true && m.Checked == false))
            {
                isCheckIn = false;
            }
            else
            {
                isCheckIn = true;
            }
            refreshCheckInOut();
            string sipRegAddress = agent.RemoteSettings.Workspace["sip.registration.domain"];
            string sipRegInterval = agent.RemoteSettings.Workspace["sip.registration.interval"];
            Properties.Settings.Default.SipRegisterDomain = sipRegAddress;
            Properties.Settings.Default.SipRegesterTimeoutSec = Convert.ToUInt32(sipRegInterval);
            showWindow();
            setLoginSuccessStyle();
            // 重配置 SIP 账号
            Sip.Utils.ReconfigAccount();
            // local ws event
            JsonRpc.Events.OnAgentLoginSucceed();
        }

        private void setLoginSuccessStyle()
        {
            logoutClick = false;
            controlBorderStatus(true);
            controlComboxStatus(true);
            controlLogoutButton(true);
            controlCheckedInOut(true);
            border_login.Visibility = Visibility.Hidden;
            tb_username.Visibility = Visibility.Visible;

        }
        private void setLogoutStyle()
        {

            Sip.Utils.Hangup();
            //  Sip.Utils.Shutdown();
            logoutClick = true;
            controlCheckedInOut(false);
            controlBorderStatus(false);
            border_login.Visibility = Visibility.Visible;
            tb_username.Visibility = Visibility.Hidden;
            controlButtonCall(false);
            controlButtonHangup(false);
            controlButtonHold(false);
            controlButtonHoldList(false);
            controlButtonInvite(false);
            controlButtonPickup(false);
            controlButtonTransfer(false);
            controlComboxStatus(false);
            controlLogoutButton(false);
            refreshStatus(AgentState.None);

        }
        private void releaseSipAccount()
        {
            // SIP Un-Reigster
            var account = Sip.Utils.Account;
            if (account.isValid())
            {
                if (account.getInfo().regIsConfigured)
                {
                    // this will start unregistration process.
                    account.setRegistration(false);
                }
            }
        }
        #region 工具条界面元素显示方式控制
        /// <summary>
        /// 摘机按钮
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlButtonPickup(bool highlight)
        {
            button_pickup.IsEnabled = highlight;
            if (highlight)
            {
                path_pickup.Fill = colorImgEnable;
                tb_pickup.Foreground = colorFontEnable;
            }
            else
            {
                path_pickup.Fill = colorImgDisable;
                tb_pickup.Foreground = colorFontDisable;
            }
        }
        /// <summary>
        /// 挂机按钮
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlButtonHangup(bool highlight)
        {

            button_hangup.IsEnabled = highlight;
            if (highlight)
            {
                path_hangup.Fill = Brushes.Red;
                tb_hangup.Foreground = colorFontEnable;
            }
            else
            {
                path_hangup.Fill = colorImgDisable;
                tb_hangup.Foreground = colorFontDisable;
            }
        }
        /// <summary>
        /// 呼叫按钮
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlButtonCall(bool highlight)
        {

            if (sipIsReg && highlight)
            {
                highlight = true;
            }
            else
            {
                highlight = false;
            }

            // TODO: 做测试！
            button_call.IsEnabled = true;// highlight;
            border_dial.IsEnabled = true;// highlight;
            if (highlight)
            {
                path_call.Fill = colorImgEnable;
                tb_call.Foreground = colorFontEnable;
            }
            else
            {
                path_call.Fill = colorImgDisable;
                tb_call.Foreground = colorFontDisable;
            }
        }
        /// <summary>
        /// 转移按钮
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlButtonTransfer(bool highlight)
        {

            button_transfer.IsEnabled = highlight;
            if (highlight)
            {
                path_transfer.Fill = colorImgEnable;
                tb_transfer.Foreground = colorFontEnable;
            }
            else
            {
                path_transfer.Fill = colorImgDisable;
                tb_transfer.Foreground = colorFontDisable;
            }
        }
        /// <summary>
        /// 邀请按钮
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlButtonInvite(bool highlight)
        {
            button_invite.IsEnabled = highlight;
            if (highlight)
            {
                path_invite1.Fill = colorImgEnable;
                path_invite2.Fill = colorImgEnable;
                path_invite3.Fill = colorImgEnable;
                path_invite4.Fill = colorImgEnable;
                path_invite5.Fill = colorImgEnable;
                path_invite6.Fill = colorImgEnable;
                tb_invite.Foreground = colorFontEnable;
            }
            else
            {
                path_invite1.Fill = colorImgDisable;
                path_invite2.Fill = colorImgDisable;
                path_invite3.Fill = colorImgDisable;
                path_invite4.Fill = colorImgDisable;
                path_invite5.Fill = colorImgDisable;
                path_invite6.Fill = colorImgDisable;
                tb_invite.Foreground = colorFontDisable;
            }
        }
        /// <summary>
        /// 保持按钮
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlButtonHold(bool highlight)
        {

            button_hold.IsEnabled = highlight;
            if (highlight)
            {
                path_hold.Fill = colorImgEnable;
                tb_hold.Foreground = colorFontEnable;
            }
            else
            {
                path_hold.Fill = colorImgDisable;
                tb_hold.Foreground = colorFontDisable;
            }
        }
        /// <summary>
        /// 保持列表
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlButtonHoldList(bool highlight)
        {

            button_holdlist.IsEnabled = highlight;
            if (highlight)
            {

                path_holdlist1.Fill = colorImgEnable;
                path_holdlist2.Fill = colorImgEnable;
                path_holdlist3.Fill = colorImgEnable;
                path_holdlist4.Fill = colorImgEnable;
                path_holdlist5.Fill = colorImgEnable;
                path_holdlist6.Fill = colorImgEnable;
                tb_holdlist.Foreground = colorFontEnable;
            }
            else
            {
                path_holdlist1.Fill = colorImgDisable;
                path_holdlist2.Fill = colorImgDisable;
                path_holdlist3.Fill = colorImgDisable;
                path_holdlist4.Fill = colorImgDisable;
                path_holdlist5.Fill = colorImgDisable;
                path_holdlist6.Fill = colorImgDisable;
                tb_holdlist.Foreground = colorFontDisable;
            }
        }


        /// <summary>
        /// 状态下拉框
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlComboxStatus(bool highlight)
        {
            var item = combox_status.Items[2] as ComboBoxItem;

            combox_status.IsEnabled = highlight;
            if (highlight)
            {

                item.Visibility = Visibility.Collapsed;
            }
            else
            {
                item.Visibility = Visibility.Visible;
            }
        }
        /// <summary>
        /// 控制状态小圆点样式
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlBorderStatus(bool highlight)
        {
            border_status_color.IsEnabled = highlight;
            if (highlight)
            {

                border_status_color.Background = Brushes.Red;
                //  border_status_color.Background = colorImgEnable;
            }
            else
            {
                border_status_color.Background = colorImgDisable;
            }
        }
        /// <summary>
        /// 控制注销按钮样式
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlLogoutButton(bool highlight)
        {
            button_logout.IsEnabled = highlight;
        }
        /// <summary>
        /// 控制签入签出按钮样式
        /// </summary>
        /// <param name="highlight"></param>
        internal void controlCheckedInOut(bool highlight)
        {
            border_check_in_out.IsEnabled = highlight;
            if (highlight)
            {
                border_check_in_out.Background = colorImgEnable;
                border_check_in_out.Cursor = Cursors.Hand;
            }
            else
            {
                border_check_in_out.Background = colorImgDisable;
                border_check_in_out.Cursor = Cursors.Arrow;
            }
        }



        #endregion


        public void getAgentInfo()
        {
            tb_username.Text = agent.DisplayName;
            tb_account.Text = agent.Name;
        }
        object lockInfoBar = new object();
        InfoBarStyle infoBarLastStyle = InfoBarStyle.Default;
        internal void setInfoBar(InfoBarStyle style, bool isTop, string msg = null)
        {
            lock (lockInfoBar)
            {
                if (infoBarLastStyle != style)
                {
                    infoBarLastStyle = style;
                    LinearGradientBrush myLinearGradientBrush = new LinearGradientBrush();
                    myLinearGradientBrush.StartPoint = new Point(0.5, 0);
                    myLinearGradientBrush.EndPoint = new Point(0.5, 1);
                    string color = null;
                    string fontColor = null;
                    switch (style)
                    {
                        case InfoBarStyle.Default:
                            color = "#FFB0BBC1";
                            fontColor = "#000000";
                            break;
                        case InfoBarStyle.Primary:
                            color = "#337ab7";
                            fontColor = "#ffffff";
                            break;
                        case InfoBarStyle.Success:
                            color = "#5cb85c";
                            fontColor = "#ffffff";
                            break;
                        case InfoBarStyle.Info:
                            color = "#5bc0de";
                            fontColor = "#ffffff";
                            break;

                        case InfoBarStyle.Warning:
                            color = "#f0ad4e";
                            fontColor = "#ffffff";
                            break;
                        case InfoBarStyle.Danger:
                            color = "#d9534f";
                            fontColor = "#ffffff";
                            break;
                    }
                    if (!string.IsNullOrEmpty(color))
                    {
                        var c = (Color)ColorConverter.ConvertFromString(color);
                        myLinearGradientBrush.GradientStops.Add(
                            new GradientStop(c, 0.0));
                        myLinearGradientBrush.GradientStops.Add(
                            new GradientStop(c, 1));
                        bd_info.Background = myLinearGradientBrush;
                        var c1 = new SolidColorBrush(c);
                        ComboBoxItem item = combox_status.Items[2] as ComboBoxItem;
                        status_other_image1.Fill = c1;
                        status_other_image2.Fill = c1;
                        item.Foreground = c1;
                        border_status_color.Background = c1;
                    }
                    if (!string.IsNullOrEmpty(fontColor))
                    {
                        var c = new SolidColorBrush((Color)ColorConverter.ConvertFromString(fontColor));
                        tb_message.Foreground = c;
                        tb_timelen.Foreground = c;
                        tb_call_status.Foreground = c;
                    }
                }
                if (isTop)
                {
                    setInfoBarTopMsg(msg);
                }
                else
                {
                    setInfoBarBottomMsg(msg);
                }

            }
        }

        void setInfoBarTopMsg(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                stopCallTime();
                tb_message.Text = "";
                tb_message.Visibility = Visibility.Hidden;
            }
            else
            {
                startCallTime();
                tb_message.Text = msg;
                tb_message.Visibility = Visibility.Visible;

            }
        }
        void setInfoBarBottomMsg(string msg = null)
        {
            if (string.IsNullOrEmpty(msg))
            {

                tb_call_status.Text = "";
                tb_call_status.Visibility = Visibility.Hidden;
            }
            else
            {
                tb_call_status.Text = msg;
                tb_call_status.Visibility = Visibility.Visible;

            }
        }
        string label_streaminfo = string.Empty;
        static SoundPlayer ringPlayer = new SoundPlayer(Properties.Resources.ringTone);
        static bool ringPlaying = false;
        internal void RefreshCallState()
        {
            controlButtonPickup(false);
            tb_ext_call_status.Text = "无";
            if (ringPlaying)
            {
                Log.Debug("停止播放来电铃声1");
                ringPlayer.Stop();
                Log.Debug("停止播放来电铃声2");
                ringPlaying = false;
            }
            var call = Sip.Utils.CurrentCall;
            if (call == null)
            {
                tb_ext_call_status.Text = "无呼叫";
                controlButtonHangup(false);
                //setInfoBar(InfoBarStyle.Info, false);
            }
            else
            {
                var ci = call.Info;
                var num = GetNumberFromUri(ci.remoteUri);
                switch (ci.state)
                {
                    case pj.pjsip_inv_state.PJSIP_INV_STATE_INCOMING:
                        tb_ext_call_status.Text = "新来电";
                        //  setInfoBar(InfoBarStyle.Info, true, "新来电");
                        break;
                    case pj.pjsip_inv_state.PJSIP_INV_STATE_CALLING:
                        controlButtonHangup(true);
                        //setInfoBar(InfoBarStyle.Info, false, num);
                        tb_ext_call_status.Text = "呼叫中";
                        // setInfoBar(InfoBarStyle.Info, true, "呼叫中");
                        break;
                    case pj.pjsip_inv_state.PJSIP_INV_STATE_EARLY:
                        showWindow();
                        controlButtonHangup(true);
                        switch (call.Dir)
                        {
                            case Sip.CallDir.Incoming:
                                controlButtonHangup(true);
                                controlButtonPickup(true);


                                tb_ext_call_status.Text = "新来电";
                                //setInfoBar(InfoBarStyle.Info, true, "新来电");
                                //setInfoBar(InfoBarStyle.Info, false, num);
                                if (!ringPlaying && !autoAnswer)
                                {
                                    Log.Debug("开始播放来电铃声1");
                                    ringPlayer.PlayLooping();
                                    Log.Debug("开始播放来电铃声2");
                                    ringPlaying = true;
                                }
                                new Thread(() =>
                                {
                                    try
                                    {
                                        Thread.Sleep(1000);
                                        ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                                        {
                                            ToolBar.instance.answer();
                                        }));
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error(ex.ToString());
                                    }


                                }).Start();

                                break;
                            case Sip.CallDir.Outgoing:
                                controlButtonHangup(true);
                                tb_ext_call_status.Text = "对方已振铃";
                                //setInfoBar(InfoBarStyle.Info, true, "对方已振铃");
                                break;
                        }
                        break;
                    case pj.pjsip_inv_state.PJSIP_INV_STATE_CONNECTING:
                        controlButtonHangup(true);
                        tb_ext_call_status.Text = "连接中";
                        //setInfoBar(InfoBarStyle.Info, true, "连接中");
                        switch (call.Dir)
                        {
                            case Sip.CallDir.Incoming:
                                //pictureBox_CallInfo.Image = Properties.Resources.linphone_call_status_incoming;
                                break;
                            case Sip.CallDir.Outgoing:
                                //pictureBox_CallInfo.Image = Properties.Resources.linphone_call_status_outgoing;
                                break;
                        }
                        break;
                    case pj.pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED:
                        controlButtonHangup(true);
                        tb_ext_call_status.Text = "通话中";
                        //setInfoBar(InfoBarStyle.Info, true, "通话中");
                        switch (call.Dir)
                        {
                            case Sip.CallDir.Incoming:
                                break;
                            case Sip.CallDir.Outgoing:
                                break;
                        }
                        break;
                    case pj.pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED:
                        tb_ext_call_status.Text = "呼叫结束";
                        controlButtonHangup(false);
                        label_streaminfo = string.Empty;
                        break;
                    case pj.pjsip_inv_state.PJSIP_INV_STATE_NULL:
                        label_streaminfo = string.Empty;
                        tb_ext_call_status.Text = "无呼叫";
                        break;
                }
                RefreshMediaState();
            }
        }
        private string GetNumberFromUri(string uri)
        {
            string result;
            var sl = uri.Trim()
                .TrimStart(new char[] { '<' })
                .TrimEnd(new char[] { '>' })
                .Split(new char[] { '@' }, 2);
            result = sl[0];
            if (result.ToLower().StartsWith("sip:"))
            {
                result = result.Substring(4);
            }
            return result;
        }
        internal void RefreshMediaState()
        {
            var call = Sip.Utils.CurrentCall;
            if (call == null)
            {
                label_streaminfo = string.Empty;
            }
            else
            {
                var ci = call.Info;
                var medCount = ci.media.Count;

                if (medCount > 0)
                {
                    var ss = new List<string>();
                    for (int i = 0; i < medCount; i++)
                    {
                        if (ci.media[i].type == pj.pjmedia_type.PJMEDIA_TYPE_AUDIO)
                        {
                            var streamInfo = call.getStreamInfo((uint)i);
                            var protocolName = "";
                            switch (streamInfo.proto)
                            {
                                case pj.pjmedia_tp_proto.PJMEDIA_TP_PROTO_RTP_AVP:
                                    protocolName = "RTP";
                                    break;
                                case pj.pjmedia_tp_proto.PJMEDIA_TP_PROTO_RTP_SAVP:
                                    protocolName = "SRTP";
                                    break;
                                case pj.pjmedia_tp_proto.PJMEDIA_TP_PROTO_UNKNOWN:
                                    protocolName = "UNKNOWN";
                                    break;
                            }
                            ss.Add(string.Format("媒体流{1}: {0} 音频 {2}({3}). ",
                                protocolName, i + 1, streamInfo.codecName, streamInfo.codecClockRate));
                        }
                        label_streaminfo = string.Join(" ", ss);

                    }
                }
                else
                {
                    label_streaminfo = string.Empty;
                }
            }
            Log.Debug("label_streaminfo is:{0}", label_streaminfo);
        }

        bool sipIsReg = false;
        /// <summary>
        /// 刷新sip状态
        /// </summary>
        internal void setRegisterStatus()
        {
            try
            {
                sipIsReg = false;
                Log.Debug("刷新sip状态");
                var acc = Sip.Utils.Account;
                string regUser = "无";
                if (acc.isValid())
                {
                    var info = Sip.Utils.Account.Info;
                    var cfg = Sip.Utils.Account.Config;

                    if (info.regIsConfigured)
                    {
                        regUser = cfg.idUri.Split(':')[1].Split('@')[0];
                        tb_ext.Text = regUser;
                        if (info.regIsActive)
                        {
                            sipIsReg = true;
                            tb_ext_status.Text = "注册成功";
                            refreshStatus(agent.State);
                            //setInfoBar(InfoBarStyle.Info, false, "注册成功");
                            // controlButtonCall(agent.State);
                        }
                        else
                        {

                            //controlButtonCall(false);
                            switch (info.regStatus)
                            {
                                case pj.pjsip_status_code.PJSIP_SC_TRYING:
                                    tb_ext_status.Text = "正在注册";
                                    // setInfoBar(InfoBarStyle.Info, false, "SIP正在注册");
                                    break;
                                default:
                                    tb_ext_status.Text = "注册失败";
                                    // setInfoBar(InfoBarStyle.Danger, false, "SIP注册失败");
                                    break;
                            }
                        }
                    }
                    else
                    {
                        controlButtonCall(false);
                        tb_ext.Text = regUser;
                        tb_ext_status.Text = "未设置SIP账户";
                        // setInfoBar(InfoBarStyle.Warning, false, "未设置SIP账户");
                    }
                }
                else
                {
                    controlButtonCall(false);
                    tb_ext.Text = regUser;
                    tb_ext_status.Text = "未设置SIP账户";
                    // setInfoBar(InfoBarStyle.Warning, false, "未设置SIP账户");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }


        }

        /// <summary>
        /// 修改固定工具条的图钉样式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void path_fix_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ToolBar.userOperatorLog("点击固定窗口图标");
            ToolBarConfig.instance.setIsTop(!Properties.Settings.Default.isWindowTop);
        }
        internal void fixToolbar()
        {
            lock (lockFix)
            {
                if (Properties.Settings.Default.isWindowTop)
                {
                    //固定
                    setPosition();
                    path_fix.Fill = Brushes.Red;
                    RotateTransform rotateTransform = new RotateTransform(-45);   //其中180是旋转180度
                    path_fix.RenderTransform = rotateTransform;
                    Thickness thick = new Thickness(977, 2, 0, 0);
                    canvas_fix.Margin = thick;
                    border_position.IsEnabled = false;
                }
                else
                {
                    //不固定
                    path_fix.Fill = colorImgEnable;
                    RotateTransform rotateTransform = new RotateTransform(0);   //其中180是旋转180度
                    path_fix.RenderTransform = rotateTransform;
                    Thickness thick = new Thickness(977, 5, 0, 0);
                    canvas_fix.Margin = thick;
                    border_position.IsEnabled = true;

                }
            }
        }

        /// <summary>
        /// 设置工具条的在屏幕的位置
        /// </summary>
        private void setPosition()
        {
            double screenW = SystemParameters.PrimaryScreenWidth;
            double left = (screenW - this.Width) / 2;
            this.Top = 0;
            this.Left = left;
        }
        private void path_fix_MouseEnter(object sender, MouseEventArgs e)
        {
            path_fix.Fill = colorFontEnable;
        }

        private void path_fix_MouseLeave(object sender, MouseEventArgs e)
        {
            if (Properties.Settings.Default.isWindowTop)
            {
                path_fix.Fill = Brushes.Red;
            }
            else
            {
                path_fix.Fill = colorImgEnable;
            }
        }

        #region 窗口贴边自动隐藏功能

        bool isHidd = false;
        double CurrentTop { get; set; }
        //鼠标进入窗口
        private void Window_MouseEnter(object sender, MouseEventArgs e)
        {
            showWindow();
        }
        internal void showWindow()
        {
            lock (lockFix)
            {
                if (!isHidd) return;
                setWindowDisplayColor();
                while (CurrentTop < 0)
                {
                    CurrentTop += 1;
                    this.Top = CurrentTop;
                }

                isHidd = false;
            }
        }

        int displaySize = 5;//贴边时显示的像素大小
        //鼠标离开窗口
        private void Window_MouseLeave(object sender, MouseEventArgs e)
        {

            hiddenWindow(false);
        }
        /// <summary>
        /// 工具条贴边
        /// </summary>
        /// <param name="needMousePosition">是否需要获取鼠标位置</param>
        internal void hiddenWindow(bool needMousePosition = true)
        {
            lock (lockFix)
            {
                if (isHidd || Properties.Settings.Default.isWindowTop) return;
                if (popup_call.IsOpen
                    || popup_holdlist.IsOpen
                    || popup_setting.IsOpen
                    || popup_status.IsOpen
                    || popup_tooltip.IsOpen) return;

                var p1 = Mouse.GetPosition(this as FrameworkElement);

                if (needMousePosition && p1.Y <= (this.Top + 48) && (p1.X > this.Left && p1.X < this.Width))
                {
                    return;
                }
                if (this.Top == 0)
                {
                    new Thread(() =>
                    {
                        Thread.Sleep(2000);//鼠标离开多久后贴边工具条
                        if (Properties.Settings.Default.isWindowTop)
                        {
                            //在休眠期间可能存在手动点击图钉改变为固定的情况，需要在此判断
                            return;
                        }
                        this.Dispatcher.BeginInvoke(new Action(() => { CurrentTop = this.Top; }));
                        while (CurrentTop >= -this.ActualHeight + displaySize)
                        {

                            if (Properties.Settings.Default.isWindowTop)
                            {
                                //在休眠期间可能存在手动点击图钉改变为固定的情况，需要在此判断
                                showWindow();
                                break;
                            }
                            CurrentTop += -1;
                            Thread.Sleep(10);//工具条贴边时的速度
                            this.Dispatcher.BeginInvoke(new Action(() => { this.Top = CurrentTop; }));
                        }
                        setWindowHiddenColor();
                        isHidd = true;
                    }).Start();

                }
            }

        }

        #endregion



        private void border_setting_MouseEnter(object sender, MouseEventArgs e)
        {
            path_setting.Fill = colorEnter;
        }

        private void border_setting_MouseLeave(object sender, MouseEventArgs e)
        {
            path_setting.Fill = colorImgEnable;
        }



        ToolBarConfig config = new ToolBarConfig();

        internal void showToolBarConfig(int index)
        {
            if (config != null)
            {
                config.selectItem(index);
                config.ShowDialog();

            }
        }

        private void button_exit_Click(object sender, RoutedEventArgs e)
        {
            ToolBar.userOperatorLog("点击退出按钮");
            MessageBoxResult r;
            bool call = isCall();
            if (call)
            {
                r = MessageBox.Show("请结束通话后再退出", "退出", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            else
            {
                r = MessageBox.Show("确定退出?", "退出", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
            }

            if (r == MessageBoxResult.Yes)
            {
                exitApplication();
            }

        }
        private void exitApplication()
        {
            try
            {

                Log.Info("释放sip资源");
                Sip.Utils.Shutdown();
                Log.Info("程序退出");
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Log.Error("退出程序时异常：" + ex.ToString());
                Environment.Exit(0);
            }
        }

        private void border_status_color_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popup_status.IsOpen = true;
        }
        /// <summary>
        /// 设置工具条贴边时的下边框粗细
        /// </summary>
        private void setWindowHiddenColor()
        {

            border_out.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (isHidd)
                {
                    Thickness thick = new Thickness(0, 0, 0, 5);
                    border_out.BorderThickness = thick;
                }
                //LinearGradientBrush myLinearGradientBrush = new LinearGradientBrush();
                //myLinearGradientBrush.StartPoint = new Point(0.5, 0);
                //myLinearGradientBrush.EndPoint = new Point(0.5, 1);
                //myLinearGradientBrush.GradientStops.Add(
                //    new GradientStop(Colors.Yellow, 1));
                //myLinearGradientBrush.GradientStops.Add(
                //    new GradientStop(Colors.Yellow, 0.0));
                //border_out.Background = myLinearGradientBrush;

                //bd_info.Visibility = Visibility.Hidden;
                //border_position.Visibility = Visibility.Hidden;
            }));

        }
        /// <summary>
        /// 设置工具条取消贴边时的下边框精细
        /// </summary>

        private void setWindowDisplayColor()
        {
            Thickness thick = new Thickness(0, 0, 0, 0);
            border_out.BorderThickness = thick;
            //bd_info.Visibility = Visibility.Visible;
            //border_position.Visibility = Visibility.Visible;
            //LinearGradientBrush myLinearGradientBrush = new LinearGradientBrush();
            //myLinearGradientBrush.StartPoint = new Point(0.5, 0);
            //myLinearGradientBrush.EndPoint = new Point(0.5, 1);
            //myLinearGradientBrush.GradientStops.Add(
            //    new GradientStop((Color)ColorConverter.ConvertFromString("#FFE5ECF5"), 1));
            //myLinearGradientBrush.GradientStops.Add(
            //    new GradientStop((Color)ColorConverter.ConvertFromString("#FFF2F7FB"), 0.0));
            //border_out.Background = myLinearGradientBrush;
        }


        private void button_holdlist_Click(object sender, RoutedEventArgs e)
        {
            popup_holdlist.IsOpen = true;
        }


        private void Border_MouseLeftButtonUp_2(object sender, MouseButtonEventArgs e)
        {
            //合并
        }

        private void Border_MouseLeftButtonUp_3(object sender, MouseButtonEventArgs e)
        {
            //退出
        }

        private void border_status_color1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popup_holdlist.IsOpen = true;
        }

        private void setStatus()
        {
            if (combox_status.SelectedIndex == -1
                && combox_status.SelectedIndex == 2)
                return;
            AgentState status = AgentState.Idle;
            Brush b = null;

            switch (combox_status.SelectedIndex)
            {
                case 0:
                    status = AgentState.Idle;
                    b = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF44CBBD"));
                    break;
                case 1:
                    status = AgentState.Away;
                    b = Brushes.Orange;
                    break;
            }
            if (agent != null && agent.Connected)
            {
                try
                {

                    agent.SetState(status);
                    if (b != null)
                    {
                        border_status_color.Background = b;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

            }

        }


        internal void refreshStatus(AgentState status)
        {
            string statusStr = "";
            string comboxStr = "";
            int index = 2;
            Brush b = null;
            InfoBarStyle style = InfoBarStyle.Info;
            controlRingTimer(false);
            switch (status)
            {
                case AgentState.None:
                    statusStr = "未登录";
                    comboxStr = statusStr;
                    setInfoBarBottomMsg();
                    style = InfoBarStyle.Danger;
                    controlButtonCall(false);
                    break;
                case AgentState.Logged:
                    statusStr = "未就绪";
                    comboxStr = statusStr;
                    setInfoBarBottomMsg("正在尝试连接...");
                    style = InfoBarStyle.Default;
                    controlButtonCall(false);
                    setInfoBarBottomMsg();
                    break;

                case AgentState.Online:
                    statusStr = "在线";
                    comboxStr = "未就绪";
                    style = InfoBarStyle.Default;
                    controlButtonCall(false);
                    setInfoBarBottomMsg();
                    break;
                case AgentState.Idle:
                    index = 0;
                    statusStr = "示闲";
                    comboxStr = statusStr;
                    b = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF44CBBD"));
                    style = InfoBarStyle.Success;
                    setInfoBarBottomMsg();
                    controlButtonCall(true);
                    break;
                case AgentState.Away:
                    index = 1;
                    statusStr = "离开";
                    comboxStr = statusStr;
                    b = Brushes.Orange;
                    style = InfoBarStyle.Warning;
                    setInfoBarBottomMsg();
                    controlButtonCall(true);
                    break;
                case AgentState.Distributed:
                    statusStr = "有呼入请接听";
                    comboxStr = "新呼入";
                    style = InfoBarStyle.Info;
                    controlButtonCall(false);
                    break;
                case AgentState.Connecting:
                    statusStr = "话机振铃";
                    comboxStr = "振铃";
                    style = InfoBarStyle.Primary;
                    controlButtonCall(false);
                    controlRingTimer(true);
                    Log.Debug("AgentState.Connecting ->agent object:{0}", agent.ToStringObj());
                    string outlineNumber = null;
                    if (agent.AcdList[0].Type == "line/agent")
                    {
                        //呼入
                        outlineNumber = agent.AcdList[0].InitialCall.From;
                    }
                    else if (agent.AcdList[0].Type == "agent/line")
                    {
                        //外呼
                        outlineNumber = agent.AcdList[0].TargetLineTelnum;
                    }
                    setInfoBar(style, false, outlineNumber);//显示外线号码
                    break;
                case AgentState.Connected:
                    statusStr = "连接话机成功";
                    comboxStr = "通话";
                    if (agent.AcdList[agent.AcdList.Count - 1].State == AcdState.InitialAgentConnected)
                    {
                        statusStr = "正在外呼";
                        comboxStr = "外呼中";
                    }
                    style = InfoBarStyle.Primary;
                    controlButtonCall(false);
                    break;
                case AgentState.PostConnected:
                    statusStr = "话后小休";
                    comboxStr = "小休";
                    style = InfoBarStyle.Warning;
                    setInfoBarBottomMsg();
                    controlButtonCall(true);
                    break;
                case AgentState.AcdInit:
                    statusStr = "连接座席话机";
                    comboxStr = "外呼中";
                    style = InfoBarStyle.Default;
                    setInfoBarBottomMsg();
                    controlButtonCall(false);
                    break;

            }
            if (combox_status.SelectedIndex != index)
            {
                isSetStatus = false;
                combox_status.SelectedIndex = index;
            }
            ComboBoxItem item = combox_status.Items[2] as ComboBoxItem;
            var tb_status = (item.Content as StackPanel).Children[1] as TextBlock;

            tb_status.Text = comboxStr;
            if (comboxStr.Length <= 2)
            {
                tb_status.Width = 35;
            }
            else
            {
                tb_status.Width = 45;
            }

            if (index != 2 || status == AgentState.PostConnected || status == AgentState.Online)
            {
                controlComboxStatus(true);
            }
            else
            {

                controlComboxStatus(false);
            }
            setInfoBar(style, true, statusStr);
            if (b != null)
            {
                border_status_color.Background = b;
            }
            //
            // local ws event
            if (agent != null)
            {
                JsonRpc.Events.OnAgentStateChanged(status);
            }
        }

        private void button_call_Click(object sender, RoutedEventArgs e)
        {
            ToolBar.userOperatorLog("呼叫下拉框");
            popup_call.IsOpen = true;
        }

        private void popup_call_Opened(object sender, EventArgs e)
        {
            txt_calledno.Focus();

        }

        private void dg_skill_list_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = dg_skill_list.SelectedIndex; //当前行号
            if (index > -1)
            {

                displayAgentList();
                //var row = dg_skill_list.ItemContainerGenerator.ContainerFromItem(dg_skill_list.Items[index]) as DataGridRow;
                //row.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFE2E2E2"));//设置选中行的颜色
            }

        }

        private void displayAgentList()
        {
            List<Agent1> agents = new List<Agent1>();
            Random rd = new Random();
            List<string> status = new List<string>() {
                "空闲",
                "正在振铃",
                "正在呼叫",
                "正在通话",
                "离开",
                "忙碌"
            };
            for (int i = 0; i < status.Count; i++)
            {
                agents.Add(new Agent1()
                {
                    displayName = string.Format("座席{0}", rd.Next(1, status.Count + 1)),
                    status = status[rd.Next(6)]
                });
            }
            dg_agent_list.ItemsSource = agents;
        }
        /// <summary>
        /// 下拉框关闭后判断工具条是否要贴边
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void popup_Closed(object sender, EventArgs e)
        {
            hiddenWindow();
        }

        private void button_pickup_Click(object sender, RoutedEventArgs e)
        {
            ToolBar.userOperatorLog("接听");
            autoAnswer = true;
            answer();
        }
        public bool autoAnswer = false;
        public void answer()
        {
            var settings = Properties.Settings.Default;
            if (settings.AutoAnswer||autoAnswer)
            {
                controlButtonPickup(false);
                if (Sip.Utils.Answer())
                {
                    autoAnswer = false;
                }

            }

        }
        private void button_hangup_Click(object sender, RoutedEventArgs e)
        {
            ToolBar.userOperatorLog("挂机");
            controlButtonHangup(false);
            Sip.Utils.Hangup();
        }
        // DateTime lastCallTime = DateTime.Now;
        private void border_dial_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ToolBar.userOperatorLog("呼叫");
            //if (DateTime.Now.Subtract(lastCallTime).Seconds < 3)
            //{
            //    MessageBox.Show("3秒内只能呼叫一次");
            //    return;
            //}
            //lastCallTime = DateTime.Now;
            string callendo = txt_calledno.Text.Trim();
            if (string.IsNullOrEmpty(callendo))
            {
                return;
            }
            try
            {
                agent.CallOut(callendo);
            }
            catch (Exception ex)
            {
                controlButtonCall(true);
                Log.Error(ex.ToString());
            }
        }
        private bool AltDown = false;
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.SystemKey == Key.LeftAlt || e.SystemKey == Key.RightAlt)
            {
                AltDown = true;
            }
            else if (e.SystemKey == Key.F4 && AltDown)
            {
                AltDown = false;
                e.Handled = true;
            }
        }
        /// <summary>
        /// 是否要设置状态
        /// </summary>
        bool isSetStatus = true;
        private void combox_status_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isSetStatus)
            {
                ToolBar.userOperatorLog("修改状态");
                var item = combox_status.SelectedItem as ComboBoxItem;
                if (item != null)
                {
                    item.Foreground = Brushes.Black;
                }
                setStatus();
            }
            isSetStatus = true;
        }



        private void button_hold_Click(object sender, RoutedEventArgs e)
        {
            ToolBar.userOperatorLog("保持");
            //var acd = agent.AcdList.FirstOrDefault(m =>
            //    m.Talkers.Any(n =>
            //        n.Agent != null 
            //        && n.Agent.Name == agent.Name 
            //        && Convert.ToInt16(n.Attr["voice_mode"]) != 4));

            var acd = agent.AcdList.FirstOrDefault(m =>
               m.Talkers.Any((n) =>
               {
                   if (n.Agent != null && n.Agent.Name == agent.Name)
                   {
                       if (Convert.ToInt16(n.Attr["voice_mode"]) != 4)
                       {
                           return true;
                       }
                   }
                   return false;
               }));
            if (acd != null)
            {
                agent.Hold();
            }

        }

        internal void holdListAdd(AcdInfo acd)
        {
            controlButtonHoldList(true);
            UCHoldAcd holdacd = new UCHoldAcd();
            holdacd.setAcdInfo(acd);
            sp_hold_list_container.Children.Add(holdacd);
        }
        internal void holdListRemove(AcdInfo acd)
        {
            UIElement r = null;
            foreach (UIElement u in sp_hold_list_container.Children)
            {
                UCHoldAcd u1 = ((UCHoldAcd)u);
                if (u1.Acd != null && u1.Acd.Id == acd.Id)
                {
                    r = u;
                    break;
                }
            }
            if (r != null)
            {
                sp_hold_list_container.Children.Remove(r);
            }
            if (sp_hold_list_container.Children.Count == 0)
            {
                controlButtonHoldList(false);
                popup_holdlist.IsOpen = false;
                //TextBlock tb = new TextBlock();
                //tb.Text = "---无---";
                //tb.Padding = new Thickness(3);
                //sp_hold_list_container.Children.Add(tb);
            }
        }
        /// <summary>
        /// 手动点击了注销
        /// </summary>
        public bool logoutClick = false;
        internal void PerformLogout()
        {
            popup_setting.IsOpen = false;
            if (agent != null && agent.Connected)
            {
                agent.Logout();
                setLogoutStyle();
                releaseSipAccount();
            }
        }

        private void canvas_setting_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popup_setting.IsOpen = true;
        }

        /// <summary>
        /// 手动点击了注销
        /// </summary>
        private void button_logout_Click(object sender, RoutedEventArgs e)
        {
            ToolBar.userOperatorLog("点击注销按钮");
            if (agent != null && agent.Connected)
            {
                bool b = false;
                if (isCall())
                {
                    var r = MessageBox.Show("请结束通话后再注销", "注销", MessageBoxButton.OK, MessageBoxImage.Information);
                    if (r == MessageBoxResult.Yes)
                    {
                        b = true;
                    }
                    return;
                }
                else
                {
                    b = true;
                }
                if (b)
                {
                    agent.Logout();
                    setLogoutStyle();
                    releaseSipAccount();
                }

                // agent = null;
            }
        }


        private void button_config_Click(object sender, RoutedEventArgs e)
        {
            ToolBar.userOperatorLog("点击选项按钮");
            showToolBarConfig(0);
        }

        private void button_transfer_Click(object sender, RoutedEventArgs e)
        {
            ToolBar.userOperatorLog("点击转移按钮");
            // 试试看
            // ToolBar.agent.Forward();
            ToolBar.agent.Transfer();
        }


    }
}
