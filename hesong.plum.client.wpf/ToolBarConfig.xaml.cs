﻿using hesong.plum.client.wpf.Common;
using hesong.plum.client.wpf.Utils;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace hesong.plum.client.wpf
{
    /// <summary>
    /// Config.xaml 的交互逻辑
    /// </summary>
    public partial class ToolBarConfig : Window
    {
        public ToolBarConfig()
        {
            InitializeComponent();
            _instance = this;
            tb_tip.Visibility = Visibility.Hidden;
        }

        List<Media> mediaList = new List<Media>();
        string regeditPath = "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
        string regeditKey = "hesong.plum.client";
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {


                addGridData();
                cb_isTop.IsChecked = Properties.Settings.Default.isWindowTop;
                cb_auto_answer.IsChecked= Properties.Settings.Default.AutoAnswer;

                tb_loginServer.Text = Properties.Settings.Default.LoginAddress.Trim();
                tb_loginServer_port.Text = Properties.Settings.Default.LoginPort.ToString();
                cb_display_login_box.IsChecked = Properties.Settings.Default.displayLoginBox;
                RegeditHelp rh = new RegeditHelp();
                string startRunStr = rh.GetRegistryData(Registry.LocalMachine,
                    regeditPath, regeditKey);
                Log.Debug("读取到的注册表值为:" + startRunStr);
                if (string.IsNullOrEmpty(startRunStr))
                {
                    cb_startup.IsChecked = false;
                }
                else
                {
                    cb_startup.IsChecked = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
        static ToolBarConfig _instance = null;
        internal static ToolBarConfig instance
        {
            get
            {
                return _instance;
            }
        }
        internal static bool SipIsConfigurated()
        {
            bool r = true;

            if (string.IsNullOrEmpty(Properties.Settings.Default.LoginAddress))
            {
                r = false;
            }
            return r;
        }
        private void addGridData()
        {
            var settings = Properties.Settings.Default;

            if (settings.SipCodecs.Count > 0)
            {
                foreach (var codec in settings.SipCodecs)
                {
                    string[] strarr = codec.Split(',');
                    if (strarr[0].Contains("PCM")
                       || strarr[0].Contains("iLBC"))
                    {

                        mediaList.Add(new Media()
                        {

                            id = strarr.Count() < 3 ? Convert.ToInt32(strarr[1]) : Convert.ToInt32(strarr[2]),
                            code = strarr[0],
                            isEnable = strarr[1] == "0" ? false : true
                        });
                    }
                }
                mediaList = mediaList.OrderBy(m => m.id).ToList();
            }
            else
            {
                int i = 0;
                foreach (var codec in Sip.Utils.Endpoint.codecEnum())
                {
                    if (codec.codecId.Contains("PCM")
                        || codec.codecId.Contains("iLBC"))
                    {
                        i++;
                        mediaList.Add(new Media()
                        {
                            id = i,
                            code = codec.codecId.Trim(),
                            isEnable = true
                        });
                    }
                }
            }
            int j = 0;
            foreach (var a in mediaList)
            {
                j++;
                a.id = j;
            }
            dataGrid1.ItemsSource = mediaList;

        }

        private void TextBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            // (sender as TextBlock).Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF40B0FF"));
            (sender as TextBlock).FontWeight = FontWeights.Bold;
        }

        private void TextBlock_MouseLeave(object sender, MouseEventArgs e)
        {
            //(sender as TextBlock).Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF000000"));
            (sender as TextBlock).FontWeight = FontWeights.Normal;
        }
        /// <summary>
        /// 上升
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBlock_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = dataGrid1.SelectedItem as Media;
            if (item != null)
            {
                int i = dataGrid1.SelectedIndex;
                if (i > 0)
                {
                    string code = ((Media)dataGrid1.Items[i - 1]).code;
                    bool isEnable = ((Media)dataGrid1.Items[i - 1]).isEnable;
                    ((Media)dataGrid1.Items[i - 1]).code = item.code;
                    ((Media)dataGrid1.Items[i - 1]).isEnable = item.isEnable;
                    item.code = code;
                    item.isEnable = isEnable;
                    dataGrid1.SelectedIndex = i - 1;
                }
            }
        }
        /// <summary>
        /// 下降
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBlock_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            var item = dataGrid1.SelectedItem as Media;
            if (item != null)
            {
                int i = dataGrid1.SelectedIndex;
                if (i < (dataGrid1.Items.Count - 1))
                {
                    string code = ((Media)dataGrid1.Items[i + 1]).code;
                    bool isEnable = ((Media)dataGrid1.Items[i + 1]).isEnable;
                    ((Media)dataGrid1.Items[i + 1]).code = item.code;
                    ((Media)dataGrid1.Items[i + 1]).isEnable = item.isEnable;
                    item.code = code;
                    item.isEnable = isEnable;
                    dataGrid1.SelectedIndex = i + 1;
                }
            }
        }
        private void Border_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            int enableCount = 0;
            foreach (var m in dataGrid1.Items)
            {
                var m1 = m as Media;
                if (m1 != null && m1.isEnable == true)
                {
                    enableCount++;
                }
            }

            var items = dataGrid1.SelectedItems;
            foreach (var item in items)
            {
                var item1 = item as Media;
                if (item1 != null)
                {
                    if (item1.isEnable == true) {
                        if (enableCount > 1)
                        {
                            item1.isEnable = !item1.isEnable;
                        }
                        else
                        {
                            MessageBox.Show("至少要启用一种编码");
                        }
                    }
                    else
                    {
                        item1.isEnable = !item1.isEnable;
                    }
                   
                }
            }



        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            //  saveConfig();
            this.Hide();
            e.Cancel = true;

        }
        private void saveConfig()
        {
            

            RegeditHelp rh = new RegeditHelp();
            if (cb_startup.IsChecked == true)
            {
                string appPath = AppDomain.CurrentDomain.BaseDirectory;
                string exeFilePath = string.Format("{0}\\{1}", appPath, Assembly.GetEntryAssembly().Location);
                exeFilePath = Assembly.GetEntryAssembly().Location;
                rh.SetRegistryData(Registry.LocalMachine,
                    regeditPath, regeditKey, exeFilePath);
            }
            else
            {
                rh.DeleteRegist(Registry.LocalMachine,
                    regeditPath, regeditKey);
            }

            var settings = Properties.Settings.Default;
            settings.AutoAnswer = cb_auto_answer.IsChecked == true ? true : false;
            saveIsTop();
            if (cb_isTop.IsChecked == true)
            {
                ToolBar.instance.showWindow();
            }
            else
            {
                ToolBar.instance.hiddenWindow();
            }
            settings.LoginAddress = tb_loginServer.Text.Trim();
            settings.LoginPort = Convert.ToInt32(tb_loginServer_port.Text.Trim());


            settings.displayLoginBox = cb_display_login_box.IsChecked == true ? true : false;
            settings.SipCodecs.Clear();
            var items = dataGrid1.Items;
            int i = items.Count;
            foreach (var item in items)
            {
                var m = item as Media;
                string s = "";
                if (m.isEnable == true)
                {
                    s = string.Format("{0},{1},{2}", m.code, i, m.id);
                }
                else
                {
                    s = string.Format("{0},0,{1}", m.code, m.id);
                }
                i--;
                settings.SipCodecs.Add(s);
            }
            settings.Save();
        }

        private void saveIsTop()
        {
            var settings = Properties.Settings.Default;
            if (cb_isTop.IsChecked == true)
            {
                settings.isWindowTop = true;
            }
            else
            {
                settings.isWindowTop = false;
            }
            settings.Save();
            ToolBar.instance.fixToolbar();
        }
        internal void setIsTop(bool isTop)
        {
            cb_isTop.IsChecked = isTop;
            saveIsTop();
        }


        internal void selectItem(int index)
        {
            tabControl.SelectedIndex = index;
        }

        private void bt_ensure_Click(object sender, RoutedEventArgs e)
        {
            saveConfig();
            this.Hide();
            ToolBar.instance.showLoginBox();
        }

        private void bt_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void tb_loginServer_port_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tb_tip == null)
            {
                return;
            }
            int port = 0;
            if (int.TryParse(tb_loginServer_port.Text.Trim(), out port))
            {
                tb_tip.Visibility = Visibility.Hidden;
            }
            else
            {
                tb_tip.Text = "端口范围必须为:1至65535";
                tb_tip.Visibility = Visibility.Visible;
            }
        }
    }

    /// <summary>
    /// 媒体
    /// </summary>
    public class Media : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected internal virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public int id { get; set; }

        private string _code;
        /// <summary>
        /// 媒体编码
        /// </summary>
        public string code
        {
            get
            {
                return this._code;
            }
            set
            {
                this._code = value;
                OnPropertyChanged("code");
            }
        }
        private bool _isEnable { get; set; }
        public bool isEnable
        {
            get
            {
                return this._isEnable;
            }
            set
            {
                this._isEnable = value;
                if (value == true)
                {
                    isEnableColor = color_on;
                    isEnablePosition = thick_on;
                }
                else
                {
                    isEnableColor = color_off;
                    isEnablePosition = thick_off;
                }
            }
        }


        SolidColorBrush color_on = Brushes.LightGreen;// new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF37B098"));
        SolidColorBrush color_off = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD6CECE"));
        Thickness thick_on = new Thickness(15, 0, 0, 0);
        Thickness thick_off = new Thickness(0, 0, 15, 0);
        SolidColorBrush _isEnableColor;
        public SolidColorBrush isEnableColor
        {
            get { return _isEnableColor; }
            set { _isEnableColor = value; OnPropertyChanged("isEnableColor"); }
        }

        Thickness _isEnablePosition;
        public Thickness isEnablePosition
        {
            get { return _isEnablePosition; }
            set { _isEnablePosition = value; OnPropertyChanged("isEnablePosition"); }
        }

    }

}
