﻿using hesong.plum.client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace hesong.plum.client.wpf.MyClass
{
    public class Agent1:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected internal virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public string displayName { get; set; }

        private string _status;
        public string status { get {
                return this._status;
            } set { this._status = value; OnPropertyChanged("status"); } }
    }
}
