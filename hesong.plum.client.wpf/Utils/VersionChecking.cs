﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;

namespace hesong.plum.client.wpf.Utils
{

    public class VersionChecking
    {
        public static bool Start(bool isAuto = true)
        {
            try
            {

                string updateAddress = Properties.Settings.Default.updateAddress;
                if (!string.IsNullOrEmpty(updateAddress))
                {
                    string sRet = HttpDownLoad.HttpDownloadXmlFile("http://" + updateAddress + "/AutoUpdate/UpdateHistory.xml");
                    string RemoteVersion = HttpDownLoad.GetConfigInXml(sRet, "releaseIndex/release", "version");
                    string Localversion = GetCurrentVersion();
                    if (RemoteVersion != Localversion && !string.IsNullOrEmpty(RemoteVersion))
                    {
                        if (MessageBox.Show("检测到更高版本的程序。是否想要下载？", "是否更新",
                            MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            string downloadContent = HttpDownLoad.GetContentInXml(sRet, "releaseIndex/release/installer");
                            if (!string.IsNullOrEmpty(downloadContent))
                            {
                                string installFile = downloadContent.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)
                                    .FirstOrDefault(m => !string.IsNullOrWhiteSpace(m));
                                if (!string.IsNullOrEmpty(installFile))
                                {
                                    installFile = string.Format("http://{0}/AutoUpdate/{1}", updateAddress, installFile.Trim());
                                    Process.Start(new ProcessStartInfo(installFile));
                                    return true;
                                }

                            }

                        }
                    }
                    else
                    {
                        if (!isAuto)
                        {
                            MessageBox.Show("当前程序已经是最新版本，无需更新。", "更新",
                            MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return false;
        }

        private static string GetCurrentVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
    }
}
