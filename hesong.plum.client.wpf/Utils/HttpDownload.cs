﻿using hesong.plum.client.wpf.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;

namespace hesong.plum.client.wpf.Utils
{
    public class HttpDownLoad
    {
        public static bool ActionUpdate = true;
        public delegate void DownProgress(int total, int curr);
        public static event DownProgress OnDownProgress;
        /// <summary>
        /// Http下载文件
        /// </summary>
        public static string HttpDownloadXmlFile(string url)
        {
            DateTime begin = DateTime.Now;
            HttpWebRequest request =
                         (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "GET";
            //request.Credentials = new NetworkCredential(username, password);
            request.PreAuthenticate = true;
            request.Timeout = 60000;
            request.CookieContainer = new CookieContainer();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream, System.Text.Encoding.UTF8);
            string inistr = readStream.ReadToEnd();
            readStream.Close();
            return inistr;
        }
        /// <summary>
        /// Http下载文件
        /// </summary>
        public static string HttpDownloadFile(string url, string path)
        {
            // 设置参数
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            //发送请求并获取相应回应数据
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            //直到request.GetResponse()程序才开始向目标网页发送Post请求
            Stream responseStream = response.GetResponseStream();

            //创建本地文件写入流
            Stream stream = new FileStream(path, FileMode.Create);

            byte[] bArr = new byte[1024];
            int size = responseStream.Read(bArr, 0, (int)bArr.Length);
            while (size > 0)
            {
                stream.Write(bArr, 0, size);
                size = responseStream.Read(bArr, 0, (int)bArr.Length);
            }
            stream.Close();
            responseStream.Close();
            return path;
        }
        public static string GetConfigInFile(string key)
        {

            //string file = "C:\\Project\\libipsc-sip-clt\\qmt-client\\bin\\Debug\\AppConfig.xml";
            string file = System.AppDomain.CurrentDomain.BaseDirectory + "AppConfig.xml";
            XmlDocument xmlDom = new XmlDocument();//首先实例化一个XmlDocument文档对象
            xmlDom.Load(file);
            string KeyPath = string.Format("volume/{0}", key);
            if (xmlDom.SelectSingleNode(KeyPath) != null)
            {
                return xmlDom.SelectSingleNode(KeyPath).InnerText;
            }
            else
                return "";

        }
        public static void SetConfigInFile(string key, string value)
        {
            //string file = "C:\\Project\\libipsc-sip-clt\\qmt-client\\bin\\Debug\\AppConfig.xml";
            string file = System.AppDomain.CurrentDomain.BaseDirectory + "AppConfig.xml";
            XmlDocument xmlDom = new XmlDocument();//首先实例化一个XmlDocument文档对象
            xmlDom.Load(file);
            string KeyPath = string.Format("volume/{0}", key);
            if (xmlDom.SelectSingleNode(KeyPath) == null)
            {
                XmlElement xmlKey = xmlDom.CreateElement(key);
                xmlKey.InnerText = value;
                xmlDom.ChildNodes[1].AppendChild(xmlKey); ;
            }
            else
            {
                XmlNode node = xmlDom.SelectSingleNode(KeyPath);
                node.InnerText = value;
            }
            xmlDom.Save(file);
        }
        public static string GetConfigInXml(string xmlStr,string path,string key)
        {
            XmlDocument xmlDom = new XmlDocument();//首先实例化一个XmlDocument文档对象
            xmlDom.LoadXml(xmlStr);
            string KeyPath = string.Format(path);
            if (xmlDom.SelectNodes(KeyPath).Count>0)
            {
                return ((xmlDom.SelectNodes(path))[0].Attributes[key]).Value;
            }
            else
            {
                return null;
            }
        }
        public static string GetContentInXml(string xmlStr, string path)
        {
            XmlDocument xmlDom = new XmlDocument();//首先实例化一个XmlDocument文档对象
            xmlDom.LoadXml(xmlStr);
            string KeyPath = string.Format(path);
            if (xmlDom.SelectNodes(KeyPath).Count > 0)
            {
                return (xmlDom.SelectNodes(path))[0].InnerText;
            }
            else
            {
                return null;
            }
        }
        public static bool DownLoadFile(string xmlStr)
        {
            try
            {
                XmlDocument xmlDom = new XmlDocument();//首先实例化一个XmlDocument文档对象
                xmlDom.LoadXml(xmlStr);
                string KeyPath = string.Format("update/updateFiles");

                if (xmlDom.SelectSingleNode(KeyPath) != null)
                {
                    string localFileName = "";
                    string remoteFileName = "";
                    int index = 0;
                    int total = xmlDom.SelectSingleNode(KeyPath).ChildNodes.Count;
                    foreach (XmlNode node in ((xmlDom.SelectSingleNode(KeyPath)).ChildNodes))
                    {
                        if (ActionUpdate)
                        {
                            remoteFileName = node.Attributes["url"].Value;
                            localFileName = string.Format("{0}temp\\{1}", System.AppDomain.CurrentDomain.BaseDirectory, node.Attributes["path"].Value);
                            HttpDownloadFile(remoteFileName, localFileName);
                            index++;
                            if (OnDownProgress != null)
                                OnDownProgress(total, index);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return false;
            }
        }
    }
}
