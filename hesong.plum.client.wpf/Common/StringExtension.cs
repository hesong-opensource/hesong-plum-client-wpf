﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace hesong.plum.client.wpf.Common
{
    public static class StringExtension
    {
        /// <summary>
        /// 使用stringBuilder格式化较大的字符串，用于性能优化
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string FormatWith(this string format, params object[] args)
        {
            try
            {
                var capacity = format.Length + args.Where(a => a != null).Select(p => p.ToString()).Sum(p => p.Length);
                var stringBuilder = new StringBuilder(capacity);
                stringBuilder.AppendFormat(format, args);
                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                Log.Error(string.Format("错误的字符串:{0}", format));
                Log.Error(string.Format("错误的字符串1:{0}", args));
                Log.Error(string.Format(format, args));
                return "错误的日志字符串";
            }
        }

        public static string ToString(this object obj)
        {
            return Convert.ToString(obj);

        }

        public static string ToStringObj(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
