﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace hesong.plum.client.wpf.Common
{
    public static class Log
    {


        /// <summary>
        /// 写日志
        /// </summary>
        private static ILog log = LogManager.GetLogger("AppLogger");
        #region Debug

        public static void Debug(string str, params object[] args)
        {
            str = str.FormatWith(args);
            Debug(str);
        }
        public static void Debug(string str)
        {
            str= handleStr(str);
            log.Debug(str);
        }
        #endregion
        #region Info


        public static void Info(string str, params object[] args)
        {
            str = str.FormatWith(args);
            Info(str);
        }
        public static void Info(string str)
        {
            str = handleStr(str);
            log.Info(str);
        }

        #endregion
        #region Warn

        public static void Warn(string str, params object[] args)
        {
            str = str.FormatWith(args);
            Warn(str);
        }
        public static void Warn(string str)
        {
            str = handleStr(str);
            log.Warn(str);
        }

        #endregion
        #region Error

        public static void Error(string str, params object[] args)
        {
            str = handleStr(str);
            str = str.FormatWith(args);
            Error(str);
        }
        public static void Error(string str)
        {
            log.Error(str);
        }

        #endregion
        #region Fatal

        public static void Fatal(string str, params object[] args)
        {
            str = str.FormatWith(args);
            Fatal(str);
        }
        public static void Fatal(string str)
        {
            str = handleStr(str);
            log.Fatal(str);
        }
        #endregion

        private static string handleStr(string str)
        {
            return str;
        }
    }

}
