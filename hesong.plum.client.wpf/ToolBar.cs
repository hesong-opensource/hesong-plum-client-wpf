﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Interop;

using hesong.plum.client;
using hesong.plum.client.wpf.Common;
using hesong.plum.client.wpf.Properties;
using hesong.plum.client.wpf.Utils;
using System.Threading;
using hesong.plum.client.wpf.MyClass;
using System.Timers;

namespace hesong.plum.client.wpf
{
    public partial class ToolBar : Window
    {
        #region 进程间消息通信,用于实现程序运行后，再次运行程序时，显示原有程序窗口。
        public const int HWND_BROADCAST = 0xffff;
        public static readonly int MyToolBarShow = RegisterWindowMessage("MyToolBarShow");
        [DllImport("user32")]
        public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);
        [DllImport("user32")]
        public static extern int RegisterWindowMessage(string message);



        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            HwndSource sorce = PresentationSource.FromVisual(this) as HwndSource;
            sorce.AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == MyToolBarShow)
            {
                showWindow();
            }
            return IntPtr.Zero;
        }

        #endregion

        /// <summary>
        /// 用户操作日志
        /// </summary>
        /// <param name="str"></param>
        public static void userOperatorLog(string str)
        {
            Log.Info("用户操作:{0}", str);
        }

        /// <summary>
        /// 工具条信息栏样式
        /// </summary>
        public enum InfoBarStyle
        {
            Default,
            Danger,
            Success,
            Primary,
            Info,
            Warning
        }

        public static Agent agent = null;

        public static void initAgent(string username)
        {
            agent = null;
            Log.Info("初始化座席");
            string loginAddr = Properties.Settings.Default.LoginAddress.Trim();
            if (!loginAddr.StartsWith("http://"))
            {
                loginAddr = string.Format("http://{0}", loginAddr);
            }
            loginAddr = string.Format("{0}:{1}", loginAddr, Properties.Settings.Default.LoginPort);
            agent = new Agent(
                loginAddr,
                username.Trim()
            );
            agent.OnConnect += Agent_OnConnect;
            agent.OnDisconnect += Agent_OnDisconnect;
            agent.OnLogout += Agent_OnLogout;
            agent.OnStateChanged += Agent_OnStateChanged;
            agent.OnAcdStateChanged += Agent_OnAcdStateChanged;
            agent.OnAcdTalkerChanged += Agent_OnAcdTalkerChanged;
            agent.OnMessageTriggered += Agent_OnMessageTriggered;

        }

        private static void Agent_OnLogout(object sender, Agent.LogoutEventArgs e)
        {
            Log.Info("注销事件: {0}", e.Reason);
           
            try
            {
                ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                {

                    ToolBar.instance.setLogoutStyle();
                    switch (e.Code)
                    {
                        case 4000:
                            break;
                        case 4010:
                            ToolBar.instance.setInfoBarBottomMsg("当前账户被服务器强制注销");
                            break;
                        case 4011:
                            ToolBar.instance.setInfoBarBottomMsg("当前账户在其它计算机上登陆");
                            break;
                        default:
                            ToolBar.instance.setInfoBarBottomMsg(string.Format(
                                "当前账户被服务器注销. [{0}]: {1}.",
                                e.Code, e.Reason
                                ));
                            break;
                    }

                    ToolBar.instance.releaseSipAccount();

                }));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        private static void Agent_OnMessageTriggered(object sender, Agent.MessageTriggeredEventArgs e)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 每个通话发生成员变化触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Agent_OnAcdTalkerChanged(object sender, Agent.AcdTalkerChangedEventArgs e)
        {
            try
            {
                switch (e.Mode)
                {
                    case 0:
                        //成员退出
                        break;
                    case 1:
                        //成员加入
                        break;
                    case 2:
                        //成员属性变化
                        // //1听+说,2听+不说，3不听+说，4不听+不说
                        int mode = Convert.ToInt32(e.TalkerInfo.Attr["voice_mode"]);
                        if (mode == 1)
                        {
                            //todo 从保持列表中删除该通话
                            ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                            {
                                ToolBar.instance.setInfoBar(InfoBarStyle.Primary, true, "通话中");
                                ToolBar.instance.holdListRemove(e.AcdInfo);
                            }));

                        }
                        else if (mode == 4)
                        {

                            ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                            {
                                ToolBar.instance.setInfoBar(InfoBarStyle.Info, true, "保持中");
                                ToolBar.instance.holdListAdd(e.AcdInfo);
                            }));
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

        }

        private static void Agent_OnAcdStateChanged(object sender, Agent.AcdStateChangedEventArgs e)
        {
            try
            {
               
                var acd = e.AcdInfo;
                Log.Debug("acd event:{0}", acd.ToStringObj());
                if (acd == null)
                {
                    Log.Error("acd为空");
                    throw new NullReferenceException("acd不能为空");
                }
                if (acd.Type == "line/agent")
                {
                    Log.Debug("呼入acd事件");
                }
                else if (acd.Type == "agent/line")
                {
                    Log.Debug("外呼acd事件");
                }
                if (acd.State == AcdState.DIstributed)
                {
                    Log.Info("新的acd呼入");
                    //ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                    //{

                    //    ToolBar.instance.setInfoBar(InfoBarStyle.Warning, false, acd.InitialCall.From);
                    //}));

                }
                else if (acd.State == AcdState.InitialAgentConnecting)
                {
                    Log.Info("外呼前开始回呼");
                    ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                    {
                        ToolBar.instance.autoAnswer = true;
                        ToolBar.instance.answer();
                    }));

                }
                else if (acd.State == AcdState.TargetLineConnecting)
                {
                    Log.Info("开始外呼被叫");
                    //ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                    //{
                    //    ToolBar.instance.setInfoBar(InfoBarStyle.Warning, false, acd.TargetLineTelnum);
                    //}));
                }
                else if (acd.State == AcdState.Talking)
                {
                    Log.Info("开始通话");
                    ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                    {
                        ToolBar.instance.setInfoBar(InfoBarStyle.Primary, true, "正在通话");
                        ToolBar.instance.controlButtonHold(true);
                    }));

                }
                else if (acd.State == AcdState.Disposed)
                {
                    Log.Info("会话结束");
                    var tmp = agent;
                    if (agent.AcdList.Count == 0)
                    {
                        ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                        {
                            ToolBar.instance.controlButtonHold(false);
                            ToolBar.instance.controlButtonHoldList(false);
                            if (!string.IsNullOrEmpty(e.ErrorMessage))
                            {
                               // ToolBar.instance.setInfoBarBottomMsg(e.ErrorMessage);
                                ToolBar.instance.setToolTip(string.Format("连接座席分机失败: {0}",
                                    e.ErrorMessage));
                            }
                        }));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }


        }

        private static void Agent_OnLog(string str)
        {
            Log.Debug("[api] {0}", str);
        }

        private static void Agent_OnStateChanged(object sender, Agent.StateChangedEventArgs e)
        {
            try
            {
                Log.Info("收到座席状态变化事件,当前状态:{0}", agent.State);
                ToolBar.instance.Dispatcher.Invoke(new Action(() =>
                {
                    ToolBar.instance.refreshStatus(agent.State);
                }));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }


        }

        private static void Agent_OnDisconnect(object sender, EventArgs e)
        {
            try
            {
                Log.Info("websocket断开连接");

                var toolbar = ToolBar.instance;
                toolbar.Dispatcher.Invoke(new Action(() =>
                {
                    toolbar.refreshStatus(AgentState.Logged);
                }));
                string password = null;
                instance.Dispatcher.Invoke(new Action(() =>
                {
                    password = Login.Instance.txt_password.Password;
                }));
                if (!string.IsNullOrEmpty(password))
                {
                    if (ToolBar.instance.logoutClick)
                    {//手动注销后将不自动连接
                        return;
                    }
                    while (true)
                    {
                        try
                        {
                            ToolBar.agent.Login(Login.Instance.txt_password.Password);
                            if (!ToolBar.agent.Connected)
                            {
                                ToolBar.agent.Connect();
                            }

                            if (ToolBar.agent.Connected)
                            {
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex.ToString());
                        }
                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }


        }

        private static void Agent_OnConnect(object sender, EventArgs e)
        {
            try
            {
                Log.Info("websocket连接成功");
                var toolbar = ToolBar.instance;

                toolbar.Dispatcher.Invoke(new Action(() =>
                {
                    toolbar.LoginSuccess();
                    toolbar.refreshStatus(agent.State);
                }));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }


        }



        private bool isCall()
        {
            if (agent != null && agent.State == AgentState.Connected)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 刷新技能列表
        /// </summary>
        private void RefreshSkillList()
        {

            List<Skill> skills = new List<Skill>();

            //skills.Add(new Skill()
            //{
            //    name = "skill1",
            //    displayName = "技能1"
            //});
            //skills.Add(new Skill()
            //{
            //    name = "skill2",
            //    displayName = "技能2"
            //});
            //skills.Add(new Skill()
            //{
            //    name = "skill3",
            //    displayName = "技能3"
            //});
            //dg_skill_list.ItemsSource = skills;
            if (agent != null)
            {
                foreach (var skill in agent.Skills)
                {

                    skills.Add(new Skill()
                    {
                        name = skill.Name,
                        displayName = skill.DisplayName
                    });
                }
                dg_skill_list.ItemsSource = skills;
            }

        }
        
        private void setToolTip(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                popup_tooltip.IsOpen = false;
                return;
            }
            toolTipDisplayTime = DateTime.Now;
            tTooltip.Enabled = true;
            popup_tooltip.IsOpen = true;
            tb_tooltip.Text = message;
            bd_tooltip.Width = message.Length * 14;
            tb_tooltip.Width = message.Length * 14 - 4;
        }
        private void TTooltip_Elapsed(object sender, ElapsedEventArgs e)
        {
            ToolBar.instance.Dispatcher.Invoke(new Action(() =>
            {
                if (popup_tooltip.IsOpen)
                {
                    if (DateTime.Now.Subtract(toolTipDisplayTime).TotalSeconds > 5)
                    {
                        popup_tooltip.IsOpen = false;
                        tTooltip.Enabled = false;
                    }
                }
            }));
            
        }
    }
}
