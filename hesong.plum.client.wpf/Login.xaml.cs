﻿using hesong.plum.client.wpf.Common;
using hesong.plum.client.wpf.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Newtonsoft.Json.Linq;

namespace hesong.plum.client.wpf
{
    /// <summary>
    /// Login.xaml 的交互逻辑
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            instance = this;
            txt_username.Text = Properties.Settings.Default.username;
            txt_password.Password = Properties.Settings.Default.password;
            cb_remember.IsChecked = Properties.Settings.Default.RememberPwd;
            tb_tip.Text = "";
        }

        public void ResetUserPassword(string name, string password)
        {
            txt_username.Text = name;
            txt_password.Password = password;
        }

        static Login instance = null;
        internal static Login Instance
        {
            get
            {
                if (instance == null)
                    instance = new Login();
                return instance;
            }
        }

        private void grid_close_MouseEnter(object sender, MouseEventArgs e)
        {
            var grid = sender as Grid;
            if (grid != null)
            {
                grid.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF3E79D3"));
            }
          
        }

        private void grid_close_MouseLeave(object sender, MouseEventArgs e)
        {
            var grid = sender as Grid;
            if (grid != null)
            {
                grid.Background = Brushes.Transparent;
            }
        }

        private void bt_login_Click(object sender, RoutedEventArgs e)
        {
            ToolBar.userOperatorLog("登录");
            PerformLogin();
        }

        internal void PerformLogin()
        {

            instance.setTip();
            string username = null;
            string password = null;
            username = txt_username.Text.Trim();
            password = txt_password.Password.Trim();
            if (string.IsNullOrEmpty(Properties.Settings.Default.LoginAddress.Trim()))
            {
                Instance.setTip("请在选项里配置服务器登录地址");
                return;
            }
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                Instance.setTip("用户名或密码不能为空");
                return;
            }
            bt_login.IsEnabled = false;
            Instance.setTip("登录中....");
            new Thread(() =>
            {
                try
                {
                    ToolBar.initAgent(username);
                    Log.Info("尝试登录,用户名:{0}", username);
                    ToolBar.agent.Login(password);
                    Instance.Dispatcher.Invoke(new Action(() =>
                    {
                        ToolBar.instance.refreshStatus(AgentState.Logged);
                    }));
                    if (!ToolBar.agent.Connected)
                        ToolBar.agent.Connect();
                    if (ToolBar.agent.Connected)
                    {
                        Instance.Dispatcher.Invoke(new Action(() =>
                        {
                            HideWindow();
                            ToolBar.instance.refreshStatus(AgentState.Online);
                            Properties.Settings.Default.username = username;
                            Properties.Settings.Default.RememberPwd =
                            Convert.ToBoolean(cb_remember.IsChecked);
                            if (cb_remember.IsChecked == true)
                            {
                                
                                ToolBar.instance.refreshStatus(AgentState.Online);
                                Properties.Settings.Default.username = username;
                                Properties.Settings.Default.RememberPwd =
                                Convert.ToBoolean(cb_remember.IsChecked);
                                if (cb_remember.IsChecked == true)
                                {
                                    Properties.Settings.Default.password = password;
                                }
                                else
                                {
                                    Properties.Settings.Default.password = string.Empty;
                                }
                                Properties.Settings.Default.SipRegisterUser = username;
                                Properties.Settings.Default.SipRegisterPassword = password;
                                Properties.Settings.Default.Save();
                                Log.Info("登录成功");
                                ToolBar.instance.LoginSuccess();
                            }
                        }));
                    }
                    else
                    {
                        Instance.Dispatcher.Invoke(new Action(() =>
                        {
                            Instance.setTip("登录成功后连接服务器失败");
                            bt_login.IsEnabled = true;
                        }));
                    }
                }
                catch (Exception ex)
                {
                    
                    Instance.Dispatcher.Invoke(new Action(() =>
                    {
                       
                        
                        bt_login.IsEnabled = true;
                        Instance.setTip("登录失败:" + ex.Message);
                        //if (ex.Message == "无法连接到远程服务器")
                        //{
                        //    Instance.setTip("服务器不可达，点击检查服务器配置地址");
                        //    showConfig = true;
                        //    tb_tip.MouseLeftButtonUp += Tb_tip_MouseLeftButtonUp;
                        //    tb_tip.MouseEnter += Tb_tip_MouseEnter;
                        //    tb_tip.MouseLeave += Tb_tip_MouseLeave;
                        //    tb_tip.Cursor = Cursors.Hand;
                        //}
                        //else
                        //{
                        //    Instance.setTip("登录失败:" + ex.Message);
                        //}
                    }));
                    Log.Error(ex.ToString());
                    // local ws event
                    JsonRpc.Events.OnAgentLoginFailed(ex.Message);
                }
            }).Start();


        }

        //private void Tb_tip_MouseLeave(object sender, MouseEventArgs e)
        //{
        //    tb_tip.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFF0000"));
        //}

        //private void Tb_tip_MouseEnter(object sender, MouseEventArgs e)
        //{
        //    tb_tip.Foreground = Brushes.SkyBlue;
        //}

        //bool showConfig = false;
        //private void Tb_tip_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    if (showConfig)
        //    {
        //        HideWindow();
        //        ToolBar.instance.showToolBarConfig(0);
               
        //    }
        //}
        private void HideWindow()
        {
            setTip();
            Hide();
        }
        private void setTip(string msg = null)
        {
            if (string.IsNullOrEmpty(msg))
            {
                tb_tip.Visibility = Visibility.Hidden;
                //showConfig = false;
                //tb_tip.Cursor = Cursors.Arrow;
                //tb_tip.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFF0000"));
                //tb_tip.MouseLeftButtonUp -= Tb_tip_MouseLeftButtonUp;
                //tb_tip.MouseEnter -= Tb_tip_MouseEnter;
                //tb_tip.MouseLeave -= Tb_tip_MouseLeave;
            }
            else
            {
                tb_tip.Visibility = Visibility.Visible;
            }
            tb_tip.Text = msg;
        }

        private void grid_titlebar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            base.OnMouseLeftButtonDown(e);

            // 获取鼠标相对标题栏位置  
            Point position = e.GetPosition(grid_titlebar);


            Point p1 = e.GetPosition(grid_close);
            double x = p1.X;
            double y = p1.Y;
            if (x > 0 && x < 16 && y > 0 && y < 16)
            {
                //如果鼠标在关闭图标内，不能拖动，否则关闭图标绑定的grid_close_MouseLeftButtonUp无法触发。
                return;
            }
            if (x < 0 && x > -23 && y > 0 && y < 16)
            {
                //如果鼠标在设置图标内，不能拖动，否则关闭图标绑定的grid_setting_MouseLeftButtonUp无法触发。
                return;
            }
            // 如果鼠标位置在标题栏内，允许拖动  
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (position.X >= 0 && position.X < grid_titlebar.ActualWidth && position.Y >= 0 && position.Y < grid_titlebar.ActualHeight)
                {
                    this.DragMove();
                }
            }
        }

      
        private void grid_close_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Hide();
          //  ToolBar.instance.hiddenWindow();
        }
        
        private void window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                PerformLogin();
            }
        }

        private void grid_setting_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
          //  Hide();
            ToolBar.instance.showToolBarConfig(0);
        }
        internal void show()
        {
            txt_username.Focus();
            instance.Hide();
            instance.ShowDialog();
        }
    }
}
