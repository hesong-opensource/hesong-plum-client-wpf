﻿using hesong.plum.client.wpf.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace hesong.plum.client.wpf
{
    /// <summary>
    /// UCHoldAcd.xaml 的交互逻辑
    /// </summary>
    public partial class UCHoldAcd : UserControl
    {
        public UCHoldAcd()
        {
            InitializeComponent();
            #region 禁用退出按钮
            border_exit.Visibility = Visibility.Collapsed;
            Width = Width - 52;
            ToolBar.instance.grid_holdlist.Width = 225;
            #endregion
        }

        public void setAcdInfo(AcdInfo acd)
        {
            Log.Debug("hold acd:{0}", acd.ToStringObj());
            Acd = acd;
            string telStr = "";
            Log.Debug("保持列表的外线号码为:{0}", acd.TargetLineTelnum);
            if (!string.IsNullOrWhiteSpace(acd.TargetLineTelnum))
            {
                telStr = acd.TargetLineTelnum;
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(acd.TargetAgentName))
                {
                    telStr = acd.TargetAgentName;
                }
            }
            tb_Telephone.Text = telStr;

        }

        public AcdInfo Acd = null;
        private void hold_cancel(object sender, MouseButtonEventArgs e)
        {
            ToolBar.userOperatorLog("找回");
            //if (Acd == null)
            //{
            //    return;
            //}
            ToolBar.agent.UnHold(Acd.Id);
        }
        SolidColorBrush colorEnter = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF3E79D3"));//鼠标进入相关控件后的颜色
        private void border_check_in_out_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Border).Background = colorEnter;

        }
        SolidColorBrush colorImgEnable = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF40B0FF"));//登录成功后矢量图的颜色
        private void border_check_in_out_MouseLeave(object sender, MouseEventArgs e)
        {

            (sender as Border).Background = colorImgEnable;
        }

        private void Border_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ToolBar.userOperatorLog("退出");
            if (Acd == null)
            {
                return;
            }
            ToolBar.instance.holdListRemove(Acd);
        }
    }
}
