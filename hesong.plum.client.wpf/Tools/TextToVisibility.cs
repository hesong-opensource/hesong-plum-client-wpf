﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace hesong.plum.client.wpf.Tools
{
    /// <summary>
    /// 文本内容转换水印的可见性
    /// </summary>
    public class TextToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //转换后的结果
            Visibility visible = Visibility.Visible;
            if (value != null)
            {
                string text = value.ToString();
                if (!string.IsNullOrEmpty(text.Trim()))
                {
                    int length = text.Length;
                    if (length > 0)
                    {
                        visible = Visibility.Collapsed;
                    }
                }
            }
            return visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return string.Empty;
        }
    }
}
