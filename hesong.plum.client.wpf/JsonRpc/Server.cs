﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Fleck;
using AustinHarris.JsonRpc;

namespace hesong.plum.client.wpf.JsonRpc
{
    class Server
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static WebSocketServer wsServer = new WebSocketServer(string.Format(
            "ws://0.0.0.0:{0}",
            Properties.Settings.Default.LocalHttpPort
        ));
        private List<IWebSocketConnection> socks = new List<IWebSocketConnection>();

        static object[] services = new object[] { new Services() };

        private static Server instance = null;
        public static Server Instance
        {
            get
            {
                if (instance == null)
                    instance = new Server();
                return instance;
            }
        }

        public void Startup()
        {
            log.Info(">>> Startup()");
            FleckLog.Level = LogLevel.Error;
            if (log.IsWarnEnabled)
                FleckLog.Level = LogLevel.Warn;
            if (log.IsInfoEnabled)
                FleckLog.Level = LogLevel.Info;
            if (log.IsDebugEnabled)
                FleckLog.Level = LogLevel.Debug;
            FleckLog.LogAction = (level, message, ex) =>
            {
                switch (level)
                {
                    case LogLevel.Debug:
                        log.Debug(message, ex);
                        break;
                    case LogLevel.Error:
                        log.Error(message, ex);
                        break;
                    case LogLevel.Warn:
                        log.Warn(message, ex);
                        break;
                    default:
                        log.Info(message, ex);
                        break;
                }
            };

            wsServer.Start(socket =>
            {
                socket.OnOpen = () =>
                {
                    log.DebugFormat("OnOpen: socket={0}", socket);
                    socks.Add(socket);
                };

                socket.OnClose = () =>
                {
                    log.DebugFormat("OnClose: socket={0}", socket);
                    socks.Remove(socket);
                };

                socket.OnMessage = message =>
                {
                    log.DebugFormat("OnMessage: socket={0}, message={1}", socket, message);
                    var rpcResultHandler = new AsyncCallback(ar =>
                    {
                        socks.ToList().ForEach(s =>
                        {
                            s.Send(((JsonRpcStateAsync)ar).Result);
                        });
                    });
                    var async = new JsonRpcStateAsync(rpcResultHandler, null)
                    {
                        JsonRpc = message
                    };
                    JsonRpcProcessor.Process(async);
                };
            });

            log.InfoFormat("<<< Startup(). WS Location={0}", wsServer.Location);
        }

        internal List<IWebSocketConnection> Socks
        {
            get
            {
                return socks;
            }
        }

    }
}
