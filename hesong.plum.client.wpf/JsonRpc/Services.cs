﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;


using AustinHarris.JsonRpc;
using Newtonsoft.Json.Linq;
using System.Windows.Input;

namespace hesong.plum.client.wpf.JsonRpc
{
    class Services : JsonRpcService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static bool RunInMainWindow(Action action)
        {
            Exception err = null;
            Window mainWindow = ToolBar.instance;
            try
            {
                mainWindow.Dispatcher.Invoke(action);
            }
            catch (TargetInvocationException ex)
            {
                err = ex.InnerException;
                log.Error("InnerException in runInMainForm()", err);
            }
            if (err != null)
            {
                if (err is org.pjsip.pjsua2.RuntimeException)
                {
                    JsonRpcContext.SetException(new JsonRpcException((err as org.pjsip.pjsua2.RuntimeException).status, err.Message, err.Data));
                }
                else
                {
                    JsonRpcContext.SetException(new JsonRpcException(-32500, err.Message, err.Data));
                }
            }
            return err == null;
        }

        [JsonRpcMethod("echo")]
        string Echo(string message)
        {
            log.DebugFormat("Echo(message={0})", message);
            return message;
        }

        [JsonRpcMethod("showLoginBox")]
        void ShowLoginBox(string user, string password)
        {
            RunInMainWindow(new Action(() =>
            {
                if (ToolBar.agent != null && ToolBar.agent.Connected)
                    return;
                Login.Instance.ResetUserPassword(user, password);
                Login.Instance.PerformLogin();
                Login.Instance.show();
            }));
        }

        [JsonRpcMethod("get")]
        JToken Get()
        {
            return new JObject(
                new JProperty("agent", Events.AgentToJson())
                );
        }

        [JsonRpcMethod("logout")]
        void Logout()
        {
            RunInMainWindow(new Action(() =>
            {
                ToolBar.instance.PerformLogout();
            }));
        }

        [JsonRpcMethod("getAgentState")]
        int? GetAgentState()
        {
            if (ToolBar.agent == null)
                return null;
            else
                return (int)ToolBar.agent.State;
        }

        [JsonRpcMethod("setAgentStatus")]
        void SetAgentStatus(int status)
        {
            int index = -1;
            RunInMainWindow(new Action(() =>
            {
                AgentState state = (AgentState)status;
                switch (state)
                {
                    case AgentState.Idle:
                        index = 0;
                        break;
                    case AgentState.Away:
                        index = 1;
                        break;
                }
                ToolBar.instance.combox_status.SelectedIndex = index;
            }));
        }

        [JsonRpcMethod("pickUp")]
        void Pickup()
        {
            RunInMainWindow(new Action(() =>
            {
                ToolBar.instance.button_pickup.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }));
        }

        [JsonRpcMethod("pickDown")]
        void PickDown()
        {
            RunInMainWindow(new Action(() =>
            {
                ToolBar.instance.button_hangup.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }));
        }

        [JsonRpcMethod("callOut")]
        void CallOut(string num)
        {
            if (string.IsNullOrWhiteSpace(num))
                throw new ArgumentNullException("num");
            RunInMainWindow(new Action(() =>
            {
                var window = ToolBar.instance;
                window.txt_calledno.Text = num;
                //. window.border_dial.RaiseEvent(new RoutedEventArgs(Border.MouseLeftButtonUpEvent));
                MouseButtonEventArgs e = new MouseButtonEventArgs(Mouse.PrimaryDevice, 0, MouseButton.Left)
                {
                    RoutedEvent = Border.MouseLeftButtonUpEvent
                };
                window.border_dial.RaiseEvent(e);
            }));
        }

        [JsonRpcMethod("pinWindow")]
        void PinWindow(bool pin)
        {
            RunInMainWindow(new Action(() =>
            {
                ToolBarConfig.instance.setIsTop(pin);
                //ToolBar.instance.fixToolbar(!pin);
            }));
        }

        [JsonRpcMethod("checkInSkill")]
        void CheckInSkill(string skill)
        {
            if (string.IsNullOrWhiteSpace(skill))
                throw new ArgumentNullException("skill");
            RunInMainWindow(new Action(() =>
            {
                var agent = ToolBar.agent;
                if (agent == null)
                    return;
                agent.CheckInSkill(skill);
            }));
        }

        [JsonRpcMethod("checkOutSkill")]
        void CheckOutSkill(string skill)
        {
            if (string.IsNullOrWhiteSpace(skill))
                throw new ArgumentNullException("skill");
            RunInMainWindow(new Action(() =>
            {
                var agent = ToolBar.agent;
                if (agent == null)
                    return;
                agent.CheckOutSkill(skill);
            }));
        }

    }
}
