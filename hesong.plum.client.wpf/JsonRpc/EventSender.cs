﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

namespace hesong.plum.client.wpf.JsonRpc
{
    class EventSender
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static void Send(IList<Fleck.IWebSocketConnection> socks, JObject request)
        {
            var reqStr = request.ToString(Formatting.None, new JavaScriptDateTimeConverter());
            socks.ToList().ForEach(s =>
            {
                log.DebugFormat("send to {0}: {1}", s, reqStr);
                s.Send(reqStr);
            });
        }

        internal static void Send(string method, JArray parameters)
        {
            var socks = Server.Instance.Socks;
            if (socks.Count() == 0)
                return;
            var reqObj = new JObject(
                new JProperty("id", ""),
                new JProperty("method", method),
                new JProperty("params", parameters)
            );
            Send(socks, reqObj);
        }

        internal static void Send(string method, JObject parameters)
        {
            var socks = Server.Instance.Socks;
            if (socks.Count() == 0)
                return;
            var reqObj = new JObject(
                new JProperty("id", ""),
                new JProperty("method", method),
                new JProperty("params", parameters)
            );
            Send(socks, reqObj);
        }

        internal static void Send(string method)
        {
            Send(method, new JObject());
        }
    }
}
