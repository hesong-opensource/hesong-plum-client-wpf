﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace hesong.plum.client.wpf.JsonRpc
{
    class Events
    {
        internal static JToken AgentToJson()
        {
            var agent = ToolBar.agent;
            var result = new JObject(
                new JProperty("name", agent.Name),
                new JProperty("displayName", agent.DisplayName),
                new JProperty("state", (int)agent.State),
                new JProperty("acdList", JArray.FromObject(agent.AcdList)),
                new JProperty("skills", JArray.FromObject(agent.Skills))
                );
            return result;
        }
        
        internal static void OnAgentLoginSucceed()
        {
            EventSender.Send(
                "onAgentLoginSucceed",
                new JObject(
                    new JProperty("agent", AgentToJson())
                ));
        }

        static internal void OnAgentLoginFailed(string message)
        {
            EventSender.Send(
                "onAgentLoginFailed",
                new JObject(
                    new JProperty("message", message)
                ));
        }

        static internal void OnAgentStateChanged(AgentState state)
        {
            EventSender.Send(
                "onAgentStateChanged",
                new JObject(
                    new JProperty("agent", AgentToJson())
                ));
        }
    }
}
