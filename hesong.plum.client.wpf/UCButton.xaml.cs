﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace hesong.plum.client.wpf
{
    /// <summary>
    /// UCButton.xaml 的交互逻辑
    /// </summary>
    public partial class UCButton : UserControl
    {
        public UCButton()
        {
            InitializeComponent();
            SetImage();
        }
        

        private void SetImage() {
            if (IsEnabled)
            {
                EnableImage();
            }
            else
            {
                DisableImage();
            }
            if (image1.Source == null)
            {
                image1.Visibility = Visibility.Collapsed;
            }
            else
            {
                image1.Visibility = Visibility.Visible;
            }
        }
        /// <summary>
        /// 设置图片
        /// </summary>
        /// <param name="element"></param>
        public void SetImage(System.Drawing.Bitmap enableImg, System.Drawing.Bitmap disableImg)
        {
            imgEnable = BitmapToBitmapImage(enableImg);
            imgDisable = BitmapToBitmapImage(disableImg);
            SetImage();
        }
        private BitmapImage BitmapToBitmapImage(System.Drawing.Bitmap bitmap)
        {
            BitmapImage bitmapImage = new BitmapImage();

            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                bitmap.Save(ms, bitmap.RawFormat);
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = ms;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
            }

            return bitmapImage;
        }
        BitmapImage imgEnable = null;
        BitmapImage imgDisable = null;
        /// <summary>
        /// 清空图片
        /// </summary>
        public void RemoveImage()
        {
            image1.Source = null;
            image1.Visibility = Visibility.Collapsed;
        }
        private void DisableImage() {
            image1.Source = imgDisable;
        }

        private void EnableImage()
        {
            image1.Source = imgEnable;
        }
        public static new readonly DependencyProperty IsEnabledProperty =
             DependencyProperty.Register("IsEnabled", typeof(bool), typeof(UCButton),
             new FrameworkPropertyMetadata(true, new PropertyChangedCallback(IsEnabledPropertyChangedCallback)));
        [Description("是否启用")]
        [Category("自定义属性")]
        public new bool IsEnabled
        {
            get
            {
                return (bool)this.GetValue(IsEnabledProperty);
            }
            set
            {
                this.SetValue(IsEnabledProperty, value);
            }
        }
        private void setBaseIsEnabled(bool b)
        {
            base.IsEnabled = b;
        }
        private static void IsEnabledPropertyChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs arg)
        {
           
            if (sender != null && sender is UCButton)
            {
                UCButton button = sender as UCButton;
                bool NewValue = (bool)arg.NewValue;
                if (NewValue)
                {
                    button.TextBlock1.Foreground = Brushes.Black;
                    button.Border1.Background = Brushes.White;
                    
                }
                else
                {
                    
                    button.TextBlock1.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFB0BBC1"));
                    button.Border1.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFF4F4F4"));
                }
                button.setBaseIsEnabled(NewValue);
                button.SetImage();
                // button.OnIsEnabledUpdated((bool)arg.OldValue, NewValue);如果参数设置需要引发事件，则需要此代码

            }
        }
        #region 如果参数设置需要引发事件，则需要此代码

        //public static readonly RoutedEvent IsEnabledUpdatedEvent =
        //EventManager.RegisterRoutedEvent("IsEnabledUpdated",
        // RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<bool>), typeof(UCButton));
        //protected virtual void OnIsEnabledUpdated(bool oldValue, bool newValue)
        //{
        //    RoutedPropertyChangedEventArgs<bool> arg =
        //        new RoutedPropertyChangedEventArgs<bool>(oldValue, newValue, IsEnabledUpdatedEvent);
        //    this.RaiseEvent(arg);

        //}
        //[Description("IsEnable被更新后发生")]
        //public event RoutedPropertyChangedEventHandler<bool> TimeUpdated
        //{
        //    add
        //    {
        //        this.AddHandler(IsEnabledUpdatedEvent, value);
        //    }
        //    remove
        //    {
        //        this.RemoveHandler(IsEnabledUpdatedEvent, value);
        //    }
        //}
        #endregion

        public string Text
        {
            get
            {
                return TextBlock1.Text;
            }
            set
            {
                TextBlock1.Text = value;
            }
        }

        private Brush _Background = Brushes.White;
        public Brush Backgroud
        {
            get
            {
                return this._Background;
            }
            set
            {
                Border1.Background = value;
                this._Background = value;
            }
        }
        private Brush _Foreground = Brushes.Black;
        public new Brush Foreground
        {
            get
            {
                return this._Foreground;
            }
            set
            {
                TextBlock1.Background = value;
                this._Foreground = value;
            }
        }
       
        private CornerRadius _CornerRadius = new CornerRadius(0);
        public CornerRadius CornerRadius
        {
            set
            {
                Border1.CornerRadius = value;
            }
            get
            {
                return this._CornerRadius;
            }
        }

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!this.IsEnabled)
            {
                return;
            }
            Border1.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF78A8F1"));
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!this.IsEnabled)
            {
                return;
            }
            Border1.Background = Brushes.White;
        }
        public delegate void ClickHandler(object sender, RoutedEventArgs e);
        public event ClickHandler Click;
        private void Border1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!this.IsEnabled)
            {
                return;
            }
            if (this.Click != null)
            {
                this.Click(this, e);
            }
            // e.Handled = false;
        }
    }
}
