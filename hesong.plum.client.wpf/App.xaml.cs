﻿using hesong.plum.client.wpf.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace hesong.plum.client.wpf
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            
            //var dict = App.Current.Resources.MergedDictionaries;
            base.OnStartup(e);
        }

        Mutex mutex;
        public App()
        {
            
            //var uri = new Uri("/PresentationFramework.AeroLite, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35;component/themes/AeroLite.NormalColor.xaml", UriKind.Relative);
            //App.Current.Resources.Source = uri;
            Startup += new StartupEventHandler(App_Startup);
        }
        void App_Startup(object sender, StartupEventArgs e)
        {
            Current.DispatcherUnhandledException += App_OnDispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            mutex = new Mutex(true, Assembly.GetExecutingAssembly().GetType().GUID.ToString(), out bool ret);

            if (!ret)
            {
                MessageBox.Show("已有一个程序在运行");
                ToolBar.PostMessage(
                    (IntPtr)ToolBar.HWND_BROADCAST,
                    ToolBar.MyToolBarShow,
                    IntPtr.Zero,
                    IntPtr.Zero
                );
                Environment.Exit(0);
            }
            Log.Info("开始启动程序");
            Log.Info("当前版本号:{0}",
                Assembly.GetExecutingAssembly().GetName().Version.ToString());
            JsonRpc.Server.Instance.Startup();
        }

        /// <summary>
        /// UI线程抛出全局异常事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                Log.Error("UI线程全局异常");
                Log.Error(e.Exception.ToString());
                Log.Error(e.Exception.StackTrace);
                e.Handled = true;
            }
            catch (Exception ex)
            {
                Log.Error("不可恢复的UI线程全局异常");
                Log.Error(ex.ToString());
                MessageBox.Show("应用程序错误");
            }
        }

        /// <summary>
        /// 非UI线程抛出全局异常事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                var exception = e.ExceptionObject as Exception;
                if (exception != null)
                {
                    Log.Error("非UI线程全局异常");
                    Log.Error(exception.ToString());
                    Log.Error(exception.Message);
                    Log.Error(exception.StackTrace);
                }
            }
            catch (Exception ex)
            {
                Log.Error("不可恢复的非UI线程全局异常");
                Log.Error(ex.ToString());
                MessageBox.Show("应用程序错误");
            }
        }
    }
}
