﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using pj = org.pjsip.pjsua2;
using Newtonsoft.Json.Linq;
using hesong.plum.client.wpf.Common;
using System.Threading;

namespace hesong.plum.client.wpf.Sip
{
    class Call : pj.Call
    {

        public Call(Account acc, int call_id, CallDir callDir) : base(acc, call_id)
        {
            dir = callDir;
        }

        public Call(Account acc, CallDir callDir) : base(acc)
        {
            dir = callDir;
        }

        private CallDir dir;
        public CallDir Dir
        {
            get
            {
                return dir;
            }
        }

        private JObject json = null;
        public JObject Json
        {
            get
            {
                if (json == null)
                {
                    var status = "";
                    switch (Info.state)
                    {
                        case pj.pjsip_inv_state.PJSIP_INV_STATE_NULL:
                            status = "";
                            break;
                        case pj.pjsip_inv_state.PJSIP_INV_STATE_CALLING:
                            status = "calling";
                            break;
                        case pj.pjsip_inv_state.PJSIP_INV_STATE_INCOMING:
                            status = "incoming";
                            break;
                        case pj.pjsip_inv_state.PJSIP_INV_STATE_EARLY:
                            status = "ringing";
                            break;
                        case pj.pjsip_inv_state.PJSIP_INV_STATE_CONNECTING:
                            status = "connecting";
                            break;
                        case pj.pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED:
                            status = "confirmed";
                            break;
                        case pj.pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED:
                            status = "disconnected";
                            break;
                    }
                    json = new JObject(
                        new JProperty("id", Info.id),
                        new JProperty("dir", Dir.ToString("G").ToLower()),
                        new JProperty("remoteUri", Info.remoteUri),
                        new JProperty("localUri", Info.localUri),
                        new JProperty("status", status)
                    );
                }
                return json;
            }
        }

        private pj.CallInfo info = null;
        public pj.CallInfo Info
        {
            get
            {
                if (info == null)
                {
                    info = getInfo();
                }
                return info;
            }
        }

        public override void onCallMediaState(pj.OnCallMediaStateParam prm)
        {
            base.onCallMediaState(prm);
            Log.Debug(">>> onCallMediaState()");
            info = getInfo();
            for (int i = 0; i < info.media.Count; i++)
            {
                if (info.media[i].type == pj.pjmedia_type.PJMEDIA_TYPE_AUDIO)
                {
                    var audMed = pj.AudioMedia.typecastFromMedia(getMedia((uint)i));
                    var mgr = pj.Endpoint.instance().audDevManager();
                    audMed.startTransmit(mgr.getPlaybackDevMedia());
                    mgr.getCaptureDevMedia().startTransmit(audMed);
                }
                else
                {
                    var callOpParam = new pj.CallOpParam(true);
                    callOpParam.statusCode = pj.pjsip_status_code.PJSIP_SC_UNSUPPORTED_MEDIA_TYPE;
                    hangup(callOpParam);
                }
            }
            var toolbar = ToolBar.instance;
            toolbar.Dispatcher.Invoke(new Action(() =>
            {
                toolbar.RefreshMediaState();
            }));
            Log.Debug("<<< onCallMediaState()");
        }

        public override void onCallState(pj.OnCallStateParam prm)
        {
            base.onCallState(prm);
            Log.Debug(">>> onCallState()");
            info = getInfo();
            json = null;
            Log.Info("Call[{0}]: onCallState(state={1})", info.id, info.stateText);
            Log.Debug("localUri={0}, localContact={1}, remoteUri={2}, remoteContact={3}", info.localUri, info.localContact, info.remoteUri, info.remoteContact);
            try
            {
                // CallInfo
                if (info.id == Utils.CurrentCall.getId())
                {
                    // GUI
                    if ((Utils.CurrentCall == null) || (info.id == Utils.CurrentCall.getId()))
                    {
                        var toolbar = ToolBar.instance;
                        toolbar.Dispatcher.Invoke(new Action(() =>
                        {
                            toolbar.RefreshCallState();
                        }));
                    }
                    // RPC
                    //jsonrpc.EventSender.send("onCallState", new JObject(new JProperty("call", Json)));
                }
            }
            finally
            {
                // Dispose
                if (info.state == pj.pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED)
                {
                    if (info.id == Utils.CurrentCall.getId())
                    {
                        Utils.SetCurrentCall(null);
                    }
                    Dispose();
                }
            }
            Log.Debug("<<< onCallState()");
        }

    }
}
