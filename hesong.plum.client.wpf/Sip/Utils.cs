﻿using hesong.plum.client.wpf.Common;
using org.pjsip.pjsua2;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pj = org.pjsip.pjsua2;

namespace hesong.plum.client.wpf.Sip
{
    class Utils
    {
        static pj.Endpoint endpoint = new pj.Endpoint(); // PJSIP Endpoint
        static Account account = new Account();
        static Call currentCall = null;

        public static pj.Endpoint Endpoint
        {
            get
            {
                return pj.Endpoint.instance();
            }
        }

        public static Call CurrentCall
        {
            get
            {
                return currentCall;
            }
        }

        internal static void SetCurrentCall(Call call)
        {
            currentCall = call;
        }

        public static Account Account
        {
            get
            {
                return account;
            }
        }

        public static pj.AudDevManager AudDevManager
        {
            get
            {
                return Endpoint.audDevManager();
            }
        }

        public static bool ReconfigAccount()
        {
            Log.Debug(">>> ReconfigAccount()");
            // var settings = Properties.Settings.Default;

            account.ReConfig();
            ToolBar.instance.setRegisterStatus();
            Log.Debug("<<< ReconfigAccount() -> true");
            return true;
        }

        public static void ReconfigCodecs()
        {
            ReconfigCodecs(false);
        }

        public static void ReconfigCodecs(bool isInit)
        {
            try
            {


                Log.Debug(">>> ReconfigCodecs(isInit={0})", isInit);

                var settings = Properties.Settings.Default;
                var codecsStringList = settings.SipCodecs;
                if (codecsStringList == null)
                    codecsStringList = settings.SipCodecs = new System.Collections.Specialized.StringCollection();
                var dictConfigCodecs = new Dictionary<string, byte>();
                foreach (string s in codecsStringList)
                {
                    var sl = s.Trim().Split(new char[] { ',' }, 3).Select(i => i.Trim()).ToArray();
                    var codecid = sl[0];
                    var priority = Convert.ToByte(sl[1]);
                    dictConfigCodecs[codecid] = priority;
                }
                foreach (var codec in Sip.Utils.Endpoint.codecEnum())
                {

                    if (!codec.codecId.Contains("PCM")
                        && !codec.codecId.Contains("iLBC"))
                    {
                        codec.priority = 0;
                    }
                }
                if (codecsStringList.Count > 0)
                {
                    foreach (var cf in codecsStringList)
                    {
                        string[] strArr = cf.Split(',');
                        string code_id = strArr[0];
                        byte priority;
                        if (strArr[1] == "0")
                        {
                            priority = 0;
                        }
                        else
                        {
                            priority = Convert.ToByte(strArr[2]);
                        }
                        Endpoint.codecSetPriority(code_id, priority);
                    }
                }

                Log.Debug("<<< ReconfigCodecs()");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public static bool Answer()
        {
            if (currentCall != null)
            {

                Log.Debug(">>> Answer()");
                var callOpParam = new pj.CallOpParam(true);
                callOpParam.statusCode = pj.pjsip_status_code.PJSIP_SC_OK;
                currentCall.answer(callOpParam);
                Log.Debug("<<< Answer()");
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool Hangup()
        {
            bool r = false;
            try
            {
               
                Log.Debug(">>> Hangup()");
                var callOpParam = new pj.CallOpParam(true);
                callOpParam.statusCode = pj.pjsip_status_code.PJSIP_SC_DECLINE;
                if (currentCall != null)
                {
                    r = true;
                    currentCall.hangup(callOpParam);
                }
                Log.Debug("<<< Hangup()");
            }
            catch(Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return r;
           
        }

        public static void Startup()
        {
            try
            {
                //var status = endpoint.libGetState();
                //if(status!= pjsua_state.PJSUA_STATE_NULL)
                //{
                //    Log.Info("已经初始化endpoint");
                //    return;
                //}
               
                Log.Info(">>>sip startup()");

                Log.Debug(">>> endpoint.libCreate()");
                endpoint.libCreate();
                Log.Debug("<<< endpoint.libCreate()");

                if (!endpoint.libIsThreadRegistered())
                {
                    Log.Debug(">>> endpoint.libRegisterThread()");
                    endpoint.libRegisterThread("startup");
                    Log.Debug("<<< endpoint.libRegisterThread()");
                }

                var epCfg = new pj.EpConfig();
                epCfg.logConfig.writer = LogWriter.Instance;
                epCfg.uaConfig.maxCalls = 1;
                epCfg.uaConfig.userAgent = string.Format(
                    "pjsip-{0} {1}-{2}.{3} dotNET-{4}",
                    pj.Endpoint.instance().libVersion().full,
                    Environment.OSVersion.Platform.ToString(),
                    Environment.OSVersion.Version.Major,
                    Environment.OSVersion.Version.Minor,
                    Environment.Version.ToString()
                );
                Log.Debug("User-Agent: {0}", epCfg.uaConfig.userAgent);

                Log.Debug(">>> endpoint.libInit()");
                endpoint.libInit(epCfg);
                Log.Debug("<<< endpoint.libInit()");

                Log.Debug(">>> endpoint.libStart()");
                endpoint.libStart();
                Log.Debug("<<< endpoint.libStart()");

                var transCfg = new pj.TransportConfig();
                transCfg.port = 0;
                int transId = endpoint.transportCreate(pj.pjsip_transport_type_e.PJSIP_TRANSPORT_UDP, transCfg);
                Log.Debug("endpoint.transportCreate() -> <{0}>\"{0}\"", transId, endpoint.transportGetInfo(transId).info);

                ReconfigCodecs(true);

                Log.Info("<<<sip startup()");
            }
            catch(Exception ex)
            {
                Log.Error(ex.ToString());
            }
            
        }

        public static void Shutdown()
        {
            Log.Info(">>> shutdown()");
            endpoint.hangupAllCalls();
            Log.Debug("endpoint.libDestroy()");
            endpoint.libDestroy();
            Log.Info("<<< shutdown()");
        }
    }
}
