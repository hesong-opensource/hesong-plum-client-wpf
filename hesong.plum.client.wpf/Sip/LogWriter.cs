﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using pj=org.pjsip.pjsua2;
using hesong.plum.client.wpf.Common;

namespace hesong.plum.client.wpf.Sip
{
    class LogWriter : pj.LogWriter
    {

        public override void Dispose()
        {
            instance = null;
        }

        public override void write(pj.LogEntry entry)
        {
            string logstr=string.Format("sipLog {0}", entry.msg.Trim());
            switch (entry.level)
            {
                case 1:
                    Log.Fatal(logstr);
                    break;
                case 2:
                    Log.Error(logstr);
                    break;
                case 3:
                    Log.Warn(logstr);
                    break;
                case 4:
                    Log.Info(logstr);
                    break;
                case 5:
                    Log.Debug(logstr);
                    break;
                case 6:
                    Log.Debug(logstr);
                    break;
                default:
                    Log.Error(string.Format("Invalid PJSIP log message level {0}", entry.level));
                    Log.Warn(logstr);
                    break;
                   // throw new SystemException(string.Format("Invalid PJSIP log message level {0}", entry.level));
            }
        }

        static LogWriter instance = null;

        internal static LogWriter Instance
        {
            get
            {
                if (instance == null)
                    instance = new LogWriter();
                return instance;
            }
        }
    }
}
