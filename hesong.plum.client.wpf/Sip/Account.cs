﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using pj = org.pjsip.pjsua2;
using Newtonsoft.Json.Linq;
using hesong.plum.client.wpf.Common;
using hesong.plum.client.wpf.Utils;
using System.Windows.Threading;
using System.Threading;

namespace hesong.plum.client.wpf.Sip
{
    class Account : pj.Account
    {

        public Account() : base()
        {
            cfg = new pj.AccountConfig();
        }

        ~Account()
        {
            cfg.Dispose();
        }

        private pj.AccountConfig cfg;
        public pj.AccountConfig Config
        {
            get
            {
                return cfg;
            }
        }

        private JObject json;
        public JObject Json
        {
            get
            {
                if (isValid())
                {
                    if (json == null)
                    {
                        json = new JObject(
                            new JProperty("id", Info.id),
                            new JProperty("uri", Info.uri),
                            new JProperty("regIsActive", Info.regIsActive),
                            new JProperty("regExpiresSec", Info.regExpiresSec),
                            new JProperty("regStatus", (int)Info.regStatus),
                            new JProperty("regStatusText", Info.regStatusText),
                            new JProperty("regLastErr", Info.regLastErr)
                        );
                    }
                    return json;
                }
                else
                {
                    return null;
                }
            }
        }

        private pj.AccountInfo info = null;
        public pj.AccountInfo Info
        {
            get
            {
                if (isValid())
                {
                    if (info == null)
                    {
                        info = getInfo();
                    }
                    return info;
                }
                else
                {
                    return null;
                }
            }
        }

        public override void onRegState(pj.OnRegStateParam prm)
        {
            base.onRegState(prm);
            Log.Debug(">>> onRegState()");
            Log.Info("Account[{0}]: onRegState(code={1}, reason=\"{2}\", status={3}, expiration={4})",
                getId(), prm.code, prm.reason, prm.status, prm.expiration);
            // AccountInfo
            info = getInfo();
            json = null;
            //todo 在工具条下拉框显示sip注册状态
            var toolbar = ToolBar.instance;
            toolbar.Dispatcher.Invoke(new Action(() =>
            {
                toolbar.setRegisterStatus();
            }));

            Log.Debug("<<< onRegState()");
        }

        public override void onIncomingCall(pj.OnIncomingCallParam prm)
        {
            base.onIncomingCall(prm);
            Log.Debug(">>> onIncomingCall()");
            var call = new Call(this, prm.callId, CallDir.Incoming);
            var ci = call.Info;
            Log.Info("Account[{0}]: onIncomingCall(id={1}, remoteUri=\"{2}\", remoteContact=\"{3}\", localUri=\"{4}\", localContact=\"{5}\")",
                getId(), ci.id, ci.remoteUri, ci.remoteContact, ci.localUri, ci.localContact);
            bool acceptable = Utils.CurrentCall == null;
            var callOpParam = new pj.CallOpParam();
            if (acceptable)
            {
                Utils.SetCurrentCall(call);
                callOpParam.statusCode = pj.pjsip_status_code.PJSIP_SC_RINGING;  //180
                                                                                 // callOpParam.statusCode = pj.pjsip_status_code.PJSIP_SC_OK;  //180
            }
            else
            {
                callOpParam.statusCode = pj.pjsip_status_code.PJSIP_SC_BUSY_HERE;  //486
            }
            Log.Debug(">>> call[0].answer(state={1})", ci.id, callOpParam.statusCode);
            call.answer(callOpParam);
            Log.Debug("<<< call[0].answer()", ci.id);
            var toolbar = ToolBar.instance;
            if (acceptable)
            {
                
                toolbar.Dispatcher.Invoke(new Action(() =>
                {
                    toolbar.RefreshCallState();
                }));
            }
            //new Thread(() =>
            //{
            //    Thread.Sleep(1000);
            //    toolbar.Dispatcher.Invoke(new Action(() =>
            //    {
            //        toolbar.answer();
            //    }));
            //}).Start();

            Log.Debug("<<< onIncomingCall()");
        }

        public void ReConfig()
        {
            try
            {
                Log.Debug(">>> ReConfig()");
                var settings = Properties.Settings.Default;
                if (string.IsNullOrEmpty(settings.SipRegisterDomain))
                {
                    throw new ApplicationException("SipRegisterConfigurated not equals true in settings.");
                }
                var needCreate = true;
                if (isValid())
                    needCreate = !Info.regIsConfigured;
                cfg.idUri = string.Format("sip:{0}@{1}", settings.SipRegisterUser, settings.SipRegisterDomain);
                cfg.regConfig.registrarUri = string.Format("sip:{0}", settings.SipRegisterDomain);
                cfg.regConfig.timeoutSec = settings.SipRegesterTimeoutSec;
                cfg.sipConfig.authCreds.Clear();
                cfg.sipConfig.authCreds.Add(new pj.AuthCredInfo("digest", "*", settings.SipRegisterUser, 0,
                    settings.SipRegisterPassword));
                cfg.callConfig.timerMinSESec = 90;
                cfg.callConfig.timerSessExpiresSec = 1800;
                if (needCreate)
                {
                    Log.Info("create SIP Account[{0}]: {1}", getId(), cfg.idUri);
                    create(cfg);
                }
                else
                {
                    Log.Info("modify SIP Account[{0}]: {1}", getId(), cfg.idUri);
                    modify(cfg);
                }
                setRegistration(true);
                Log.Debug("<<< ReConfig()");
            }
            catch (Exception ex)
            {
                
                Log.Error(ex.ToString());
            }

        }

    }
}
