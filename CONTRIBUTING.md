# CONTRIBUTING

这个程序依赖于 `hesong_plum_agent_client-net40`，且项目文件中，这个依赖的路径是既定的。

所以，在将本项目加入到解决方案的时候，应保证如下的目录结构：

```
工作目录/
  ├── 解决方案目录/
  │    └── xxx.sol
  │    └── packages/
  │        ├── log4net/
  │        ├── Newtonsoft.Json/
  │        └── .....
  │
  ├── hesong-plum-client-sdk-csharp/
  │    └── projects/
  │        ├── hesong_plum_agent_client-net40/
  │        │    ├── hesong_plum_agent_client-net40.csproj
  │        │    ├── packages.config
  │        │    └── ...
  │        └── src/
  │
  └── hesong-plum-client-wpf/
      └── hesong.plum.client.wpf/
          ├── hesong.plum.client.wpf.csproj
          ├── packages.config
          └── ...
```

`hesong-plum-client-wpf` 项目的依赖包是手动写在项目文件中的，形如：

```xml
<Reference Include="log4net, Version=2.0.8.0, Culture=neutral, PublicKeyToken=669e0ddf0bb1aa2a, processorArchitecture=MSIL">
  <HintPath>$(SolutionDir)\log4net.2.0.8\lib\net40-full\log4net.dll</HintPath>
  <Private>True</Private>
</Reference>
```

为了防止 NuGet 的包管理器的路径问题，所有的引用的 `HintPath` 都用IDE环境变量 `$(SolutionDir)`。
如果添加新的依赖包，应做类似的手动修改。
